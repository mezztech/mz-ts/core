#!/usr/bin/env bash

function permit() {
    ### Recursively claim ownership of all project files and folders
    sudo chown -R $(whoami) $(pwd);
    ### Recursively set permissions to read/write/execute on all project files and folders
    sudo chmod -R g+w $(pwd);
    return 0;
}
