#!/bin/bash

# Using force-with-lease tells git to check whether the remote
# repo is the same as the one you’re trying to push up. If it
# isn’t, git will throw an error instead of blindly overwriting
# the remote repo. This will save you from accidentally
# overwriting work that you don’t intend to.

# When you do a rebase on your branch to get it ready for merging,
# you’ll need to force push the branch to origin if you’ve squashed
# the commits and changed the history.

git push --force-with-lease origin master
