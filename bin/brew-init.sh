#!/bin/bash

#==================================================
# Get the directory of the script file
#==================================================
# Using pwd alone will not work if you are not running the script from
# the directory it is contained in.
#
# $() acts as a kind of quoting for commands but they're run in their own context.
# dirname gives you the path portion of the provided argument (removing the file key).
#
# This command gets the script's source file pathname, strips it to just
# the path portion, cds to that path, then uses pwd to return the (effectively)
# full path of the script. This is assigned to the variable. After all of
# that, the context is unwound so you end up back in the directory you started
# at but with an environment variable containing the script's path.
dirScript="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source $dirScript/util/util-spinner.sh

#==================================================
# Make sure all the necessary tools are installed
#==================================================
function brew-init() {
    echo "Installing common components"

    # Install brew, if it's not already installed.
    # Otherwise, make sure it's updated
    which -s brew > /dev/null 2>&1
    if [ $? != 0 ]
    then
        echo "Installing Homebrew"
        /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" > /dev/null 2>&1 & spinner $!
    else
        echo "Updating Homebrew"
        brew update > /dev/null 2>&1 & spinner $!
    fi
}
