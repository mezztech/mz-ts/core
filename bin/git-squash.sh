#!/bin/bash

### SEE: https://medium.freecodecamp.com/bash-shortcuts-to-enhance-your-git-workflow-5107d64ea0ff

### USAGE:
### source ./git-squash.sh;
### gitSquashBranch master;

function gitCountCommitsInBranch() {
    ### Use develop as the default value, if no argument is provided
    local branchBase=${1-develop}

    ### Get the number of commits that are accessible
    ### from HEAD (the current branch) but NOT ACCESSIBLE from branchBase
    local count=$(git rev-list --count HEAD ^${branchBase})
    echo $[count - 1]
}

function gitSquashBranch() {
    local branchBase=${1-develop}
    local commitCount=$(gitCountCommitsInBranch ${branchBase})
    echo "${commitCount} commits have been made to this branch since it diverged from ${branchBase}"
    git rebase --interactive --autosquash HEAD~${commitCount}
}

function gitAutoSquashBranch() {
    local branchBase=${1-develop}
    git rebase --interactive --autosquash ${branchBase}
}
