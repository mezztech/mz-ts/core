#!/bin/bash

#==================================================
# Get the directory of the script file
#==================================================
# Using pwd alone will not work if you are not running the script from
# the directory it is contained in.
#
# $() acts as a kind of quoting for commands but they're run in their own context.
# dirname gives you the path portion of the provided argument (removing the file key).
#
# This command gets the script's source file pathname, strips it to just
# the path portion, cds to that path, then uses pwd to return the (effectively)
# full path of the script. This is assigned to the variable. After all of
# that, the context is unwound so you end up back in the directory you started
# at but with an environment variable containing the script's path.
dirScript="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source $dirScript/util/util-spinner.sh
source $dirScript/git-flow-init.sh

init() {
    echo "Making sure npm-check-updates is installed globally"
    sudo npm install -g npm-check-updates > /dev/null 2>&1 & spinner $!
}

init
