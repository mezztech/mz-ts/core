#!/bin/bash

function commit() {
    local message=${1};

    if [ "${message}" = "" ]; then
        ### Set commit to be squashed to the previous commit
        ### Call ./tasks/git-squash.sh to enact
        sudo git add . && \
        sudo git add -u && \
        sudo git commit --squash=HEAD~0;
    else
        sudo git add . && \
        sudo git add -u && \
        git commit -m "${message}" && \
        git push origin HEAD;
    fi
}
