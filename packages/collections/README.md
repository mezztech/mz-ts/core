# collections

Generic lists, queues, and stacks.

## Install

`sudo npm install --save @mezzy/collections`

## Usage

```
import {List, Queue, Stack} from '@mezzy/collections';
