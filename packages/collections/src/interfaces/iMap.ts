import CollectionTools from '../collectionTools';
import IMapPair from './iMapPair';


export interface IMap<K, V> {
    readonly size:number;


    /**
     * Returns true if this map has no mappings.
     * @return {boolean} true if this map has no mappings.
     */
    readonly isEmpty:boolean;


    /**
     * Returns an array containing all of the keys in this map.
     * @return {Array} an array containing all of the keys in this map.
     */
    readonly keys:K[];


    /**
     * Returns an array containing all of the values in this map.
     * @return {Array} an array containing all of the values in this map.
     */
    readonly values:V[];


    /**
     * Returns the value to which this map maps the specified key.
     * Returns undefined if this map has no mapping for this key.
     * @param {Object} key key whose associated value is to be returned.
     * @return {*} the value to which this map maps the specified key or undefined if the map has no mapping for this key.
     */
    get(key:K):V;


    /**
     * Associates the specified value with the specified key in this map.
     * If the map previously contained a mapping for this key, the old
     * value is replaced by the specified value.
     * @param {Object} key key with which the specified value is to be associated.
     * @param {Object} value value to be associated with the specified key.
     * @return {*} previous value associated with the specified key, or undefined if there was no mapping for the key or if the key/value are undefined.
     */
    set(key:K, value:V):V;


    /**
     * Removes the mapping for this key from this map if it is present.
     * @param {Object} key key whose mapping is to be deleted from the map.
     * @return {*} value associated with specified key, or undefined if there was no mapping for key.
     */
    delete(key:K):V;



    /**
     * Executes the provided function once for each key-value pair
     * present in this map.
     * @param {function(Object, Object):*} callback function to execute, it is
     * invoked with two arguments: value and key. To break the iteration you can
     * optionally return false.
     */
    forEach(callback:(value:V, key:K) => boolean):void;


    /**
     * Returns true if this map has a mapping for the specified key.
     * @param {Object} key key whose presence in this map is to be tested.
     * @return {boolean} true if this map has a mapping for the specified key.
     */
    has(key:K):boolean;


    /**
     * Removes all mappings from this map.
     * @this {CollectionTools.Map}
     */
    clear():void;


    toString():string;
} // End class


export default IMap;
