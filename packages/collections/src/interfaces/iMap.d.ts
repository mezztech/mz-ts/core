export interface IMap<K, V> {
    readonly size: number;
    readonly isEmpty: boolean;
    readonly keys: K[];
    readonly values: V[];
    get(key: K): V;
    set(key: K, value: V): V;
    delete(key: K): V;
    forEach(callback: (value: V, key: K) => boolean): void;
    has(key: K): boolean;
    clear(): void;
    toString(): string;
}
export default IMap;
