export interface IMapPair<K, V> {
    key: K;
    value: V;
}
export default IMapPair;
