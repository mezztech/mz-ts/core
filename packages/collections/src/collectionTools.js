"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CollectionTools {
    static defaultCompare(a, b) {
        if (a < b)
            return -1;
        else if (a === b)
            return 0;
        else
            return 1;
    }
    static defaultEquals(a, b) { return a === b; }
    static defaultToString(item) {
        if (item === null)
            return 'COLLECTION_NULL';
        else if (CollectionTools.isUndefined(item))
            return 'COLLECTION_UNDEFINED';
        else if (CollectionTools.isString(item))
            return item;
        else
            return item.toString();
    }
    static makeString(item, join = ",") {
        if (item === null)
            return 'COLLECTION_NULL';
        else if (CollectionTools.isUndefined(item))
            return 'COLLECTION_UNDEFINED';
        else if (CollectionTools.isString(item))
            return item.toString();
        else {
            let toret = "{";
            let first = true;
            for (let prop in item) {
                if (item.hasOwnProperty(prop)) {
                    if (first)
                        first = false;
                    else
                        toret = toret + join;
                    toret = toret + prop + ":" + item[prop];
                }
            }
            return toret + "}";
        }
    }
    static isFunction(func) { return (typeof func) === 'function'; }
    static isUndefined(obj) { return (typeof obj) === 'undefined'; }
    static isString(obj) { return Object.prototype.toString.call(obj) === '[object String]'; }
    static reverseCompareFunction(compareFunction) {
        if (!CollectionTools.isFunction(compareFunction)) {
            return function (a, b) {
                if (a < b)
                    return 1;
                else if (a === b)
                    return 0;
                else
                    return -1;
            };
        }
        else
            return function (d, v) { return compareFunction(d, v) * -1; };
    }
    static compareToEquals(compareFunction) {
        return function (a, b) { return compareFunction(a, b) === 0; };
    }
}
exports.CollectionTools = CollectionTools;
exports.default = CollectionTools;
//# sourceMappingURL=collectionTools.js.map