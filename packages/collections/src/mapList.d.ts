import IMap from './interfaces/iMap';
export declare class MapList<K, V> implements IMap<K, V> {
    constructor(_toStringFunction?: (key: K) => string);
    private table;
    readonly size: number;
    protected p_length: number;
    private _toString;
    get(key: K): V;
    set(key: K, value: V): V;
    delete(key: K): V;
    readonly keys: K[];
    readonly values: V[];
    forEach(callback: (value: V, key: K) => boolean): void;
    has(key: K): boolean;
    clear(): void;
    readonly isEmpty: boolean;
    toString(): string;
}
export default MapList;
