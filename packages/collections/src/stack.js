"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const list_1 = require("./list");
class Stack {
    constructor() { this._list = new list_1.default(); }
    get isEmpty() { return this._list.isEmpty; }
    push(elem) { return this._list.add(elem, 0); }
    add(elem) { return this._list.add(elem, 0); }
    pop() { return this._list.deleteAtIndex(0); }
    peek() { return this._list.first; }
    get size() { return this._list.size; }
    has(elem) { return this._list.has(elem); }
    clear() { this._list.clear(); }
    forEach(callback) { this._list.forEach(callback); }
}
exports.Stack = Stack;
exports.default = Stack;
//# sourceMappingURL=stack.js.map