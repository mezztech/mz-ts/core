"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class IntersectionPoint {
    constructor() {
        this.isOnLine1 = false;
        this.isOnLine2 = false;
    }
}
exports.IntersectionPoint = IntersectionPoint;
class Numbers {
    static r(min = 0, max = 1, isInt = false) { return Numbers.random(min, max, isInt); }
    static random(min = 0, max = 1, isInt = false) {
        let r = (Math.random() * (max - min)) + min;
        if (isInt)
            r = Math.round(r);
        return r;
    }
    static randomByte() { return Numbers.random(0, 255, true); }
    static angleToRadians(angleInDegrees) { return angleInDegrees * Math.PI / 180.0; }
    static angleToDegrees(angleInRadians) { return angleInRadians * 180.0 / Math.PI; }
    static angleRadians(x1, y1, x2, y2) {
        return Math.atan2(y2 - y1, x2 - x1);
    }
    static angle(x1, y1, x2, y2) {
        return this.angleToDegrees(this.angleRadians(x1, y1, x2, y2));
    }
    static screenToCartesian(point, screenWidth, screenHeight) {
        const xScreen = point[0];
        const yScreen = point[1];
        const xCartesian = xScreen - screenWidth / 2;
        const yCartesian = -yScreen + screenHeight / 2;
        return [xCartesian, yCartesian];
    }
    static cartesianToScreen(point, screenWidth, screenHeight) {
        const xCartesian = point[0];
        const yCartesian = point[1];
        const xScreen = xCartesian + screenWidth / 2;
        const yScreen = -yCartesian + screenHeight / 2;
        return [xScreen, yScreen];
    }
    static constrainToInterval(value, interval) {
        return Math.round(value / interval) * interval;
    }
    static round(value, decimalPlaces) { return this._decimalAdjust('round', value, -decimalPlaces); }
    static floor(value, decimalPlaces) { return this._decimalAdjust('floor', value, -decimalPlaces); }
    static ceil(value, decimalPlaces) { return this._decimalAdjust('ceil', value, -decimalPlaces); }
    static _decimalAdjust(type, value, exp) {
        if (exp == null || exp == undefined || exp != exp || exp === 0) {
            switch (type) {
                case 'round':
                    return Math.round(value);
                case 'floor':
                    return Math.floor(value);
                case 'ceil':
                    return Math.ceil(value);
            }
        }
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
            return NaN;
        let valueStringArray = value.toString().split('e');
        switch (type) {
            case 'round':
                value = Math.round(+(valueStringArray[0] + 'e' + (valueStringArray[1] ? (+valueStringArray[1] - exp) : -exp)));
                break;
            case 'floor':
                value = Math.floor(+(valueStringArray[0] + 'e' + (valueStringArray[1] ? (+valueStringArray[1] - exp) : -exp)));
                break;
            case 'ceil':
                value = Math.ceil(+(valueStringArray[0] + 'e' + (valueStringArray[1] ? (+valueStringArray[1] - exp) : -exp)));
                break;
        }
        valueStringArray = value.toString().split('e');
        return +(valueStringArray[0] + 'e' + (valueStringArray[1] ? (+valueStringArray[1] + exp) : exp));
    }
    static intersection(line1X1, line1Y1, line1X2, line1Y2, line2X1, line2Y1, line2X2, line2Y2) {
        let denominator;
        let a;
        let b;
        let numerator1;
        let numerator2;
        let intersection = new IntersectionPoint();
        denominator = ((line2Y2 - line2Y1) * (line1X2 - line1X1)) - ((line2X2 - line2X1) * (line1Y2 - line1Y1));
        if (denominator == 0)
            return intersection;
        a = line1Y1 - line2Y1;
        b = line1X1 - line2X1;
        numerator1 = ((line2X2 - line2X1) * a) - ((line2Y2 - line2Y1) * b);
        numerator2 = ((line1X2 - line1X1) * a) - ((line1Y2 - line1Y1) * b);
        a = numerator1 / denominator;
        b = numerator2 / denominator;
        intersection.x = line1X1 + (a * (line1X2 - line1X1));
        intersection.y = line1Y1 + (a * (line1Y2 - line1Y1));
        if (a > 0 && a < 1)
            intersection.isOnLine1 = true;
        if (b > 0 && b < 1)
            intersection.isOnLine2 = true;
        return intersection;
    }
    static distanceBetweenPoints(pointA, pointB) {
        if (!pointA[2])
            pointA[2] = 0;
        if (!pointB[2])
            pointB[2] = 0;
        return Math.sqrt(Math.pow(pointA[0] - pointB[0], 2) + Math.pow(pointA[1] - pointB[1], 2) + Math.pow(pointA[2] - pointB[2], 2));
    }
    static midpoint(x1, y1, x2, y2) {
        let midpoint = [];
        midpoint[0] = (x1 + x2) / 2;
        midpoint[1] = (y1 + y2) / 2;
        return midpoint;
    }
    static nearestPointOnLine(point, lineStart, lineEnd) {
        if (!point[2])
            point[2] = 0;
        if (!lineStart[2])
            lineStart[2] = 0;
        if (!lineEnd[2])
            lineEnd[2] = 0;
        const lineLength = this.distanceBetweenPoints(lineStart, lineEnd);
        let pointNearestOnLine;
        let nearestPoint;
        if (lineLength === 0) {
            pointNearestOnLine = [lineStart[0], lineStart[1], lineStart[2]];
            return [pointNearestOnLine[0], pointNearestOnLine[1], pointNearestOnLine[2], , 0];
        }
        const p = (((point[0] - lineStart[0]) * (lineEnd[0] - lineStart[0])) +
            ((point[1] - lineStart[1]) * (lineEnd[1] - lineStart[1])) +
            ((point[2] - lineStart[2]) * (lineEnd[2] - lineStart[2]))) / lineLength;
        if (p < 0) {
            pointNearestOnLine = [lineStart[0], lineStart[1], lineStart[2]];
            nearestPoint = [pointNearestOnLine[0], pointNearestOnLine[1], pointNearestOnLine[2], p];
            return nearestPoint;
        }
        if (p > 1) {
            pointNearestOnLine = [lineEnd[0], lineEnd[1], lineEnd[2]];
            nearestPoint = [pointNearestOnLine[0], pointNearestOnLine[1], pointNearestOnLine[2], p];
            return nearestPoint;
        }
        pointNearestOnLine = this.pointOnLine(p, lineStart, lineEnd);
        nearestPoint = [pointNearestOnLine[0], pointNearestOnLine[1], pointNearestOnLine[2], p];
        return nearestPoint;
    }
    static pointOnLine(p, lineStart, lineEnd) {
        if (!lineStart[2])
            lineStart[2] = 0;
        if (!lineEnd[2])
            lineEnd[2] = 0;
        const xNew = lineStart[0] + (p * (lineEnd[0] - lineStart[0]));
        const yNew = lineStart[1] + (p * (lineEnd[1] - lineStart[1]));
        const zNew = lineStart[2] + (p * (lineEnd[2] - lineStart[2]));
        return [xNew, yNew, zNew];
    }
    static pointOnLineScreen(p, lineStart, lineEnd, screenWidth, screenHeight) {
        lineStart = this.screenToCartesian(lineStart, screenWidth, screenHeight);
        lineEnd = this.screenToCartesian(lineEnd, screenWidth, screenHeight);
        const pointOnLine = this.pointOnLine(p, lineStart, lineEnd);
        return this.cartesianToScreen(pointOnLine, screenWidth, screenHeight);
    }
    static distanceToLine(point, lineStart, lineEnd) {
        if (!point[2])
            point[2] = 0;
        if (!lineStart[2])
            lineStart[2] = 0;
        if (!lineEnd[2])
            lineEnd[2] = 0;
        const pointNearestOnLine = this.nearestPointOnLine(point, lineStart, lineEnd);
        const distanceToNearestPoint = this.distanceBetweenPoints(point, pointNearestOnLine);
        return Math.pow(distanceToNearestPoint, 2);
    }
    static randomPointWithinCircle(x, y, radius) {
        const pt_angle = Math.random() * 2 * Math.PI;
        const pt_radius_sq = Math.random() * radius * radius;
        const pt_x = (Math.sqrt(pt_radius_sq) * Math.cos(pt_angle)) + x;
        const pt_y = (Math.sqrt(pt_radius_sq) * Math.sin(pt_angle)) + y;
        return [pt_x, pt_y];
    }
    static isPointInEllipse(x, y, ellipseCenterX, ellipseCenterY, radiusX, radiusY, rotation) {
        rotation = rotation || 0;
        let cos = Math.cos(rotation);
        let sin = Math.sin(rotation);
        let dx = (x - ellipseCenterX);
        let dy = (y - ellipseCenterY);
        let tdx = cos * dx + sin * dy;
        let tdy = sin * dx - cos * dy;
        return (tdx * tdx) / (radiusX * radiusX) + (tdy * tdy) / (radiusY * radiusY) <= 1;
    }
    static pointOnEllipse(angleAlongCircumference, ellipseRadiusX, ellipseRadiusY, ellipseCenterX = 0, ellipseCenterY = 0) {
        const percent = angleAlongCircumference > 1 ? angleAlongCircumference / 360 : angleAlongCircumference;
        const x = (Math.cos(2 * Math.PI * percent) * ellipseRadiusX) + ellipseCenterX;
        const y = (Math.sin(2 * Math.PI * percent) * ellipseRadiusY) + ellipseCenterY;
        return [x, y];
    }
    static rotate(x, y, angle, originX = 0, originY = 0) {
        angle = this.angleToRadians(angle);
        return this.rotateRadians(x, y, angle, originX, originY);
    }
    static rotateRadians(x, y, angle, originX = 0, originY = 0) {
        return this.rotateZ(x, y, 0, angle, originX, originY, 0);
    }
    static rotateX(x, y, z, angle, originX = 0, originY = 0, originZ = 0) {
        angle = this.angleToRadians(angle);
        return this.rotateXRadians(x, y, z, angle, originX, originY, originZ);
    }
    static rotateXRadians(x, y, z, angle, originX = 0, originY = 0, originZ = 0) {
        let cos = Math.cos(angle);
        let sin = Math.sin(angle);
        let rotatedY = (cos * (y - originY)) - (sin * (z - originZ)) + originY;
        let rotatedZ = (cos * (z - originZ)) + (sin * (y - originY)) + originZ;
        return [x, rotatedY, rotatedZ];
    }
    static rotateY(x, y, z, angle, originX = 0, originY = 0, originZ = 0) {
        angle = this.angleToRadians(angle);
        return this.rotateYRadians(x, y, z, angle, originX, originY, originZ);
    }
    static rotateYRadians(x, y, z, angle, originX = 0, originY = 0, originZ = 0) {
        let cos = Math.cos(angle);
        let sin = Math.sin(angle);
        let rotatedX = (cos * (x - originX)) - (sin * (z - originZ)) + originX;
        let rotatedZ = (cos * (z - originZ)) + (sin * (x - originX)) + originZ;
        return [rotatedX, y, rotatedZ];
    }
    static rotateZ(x, y, z, angle, originX = 0, originY = 0, originZ = 0) {
        angle = this.angleToRadians(angle);
        return this.rotateZRadians(x, y, z, angle, originX, originY, originZ);
    }
    static rotateZRadians(x, y, z, angle, originX = 0, originY = 0, originZ = 0) {
        let cos = Math.cos(angle);
        let sin = Math.sin(angle);
        let rotatedX = (cos * (x - originX)) - (sin * (y - originY)) + originX;
        let rotatedY = (cos * (y - originY)) + (sin * (x - originX)) + originY;
        return [rotatedX, rotatedY, z];
    }
    static getIndices(indexStart, indexEnd, isShuffle) {
        let currentIndex = indexEnd;
        let temporaryValue;
        let randomIndex;
        const indexArray = [];
        for (let i = indexStart; i <= indexEnd; i++) {
            indexArray.push(i);
        }
        if (!isShuffle)
            return indexArray;
        while (currentIndex !== indexStart) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex--;
            temporaryValue = indexArray[currentIndex];
            indexArray[currentIndex] = indexArray[randomIndex];
            indexArray[randomIndex] = temporaryValue;
        }
        return indexArray;
    }
    static normalize(value, min, max, isConstrain = true, isInvert = false) {
        let p = (value - min) / (max - min);
        if (isConstrain) {
            if (p < 0)
                p = 0;
            if (p > 1)
                p = 1;
        }
        if (isInvert)
            p = 1.0 - p;
        return p;
    }
    static unnormalize(valueNormalized, min, max, isConstrain = true, isInvert = false) {
        let p = valueNormalized;
        if (isConstrain) {
            if (p < 0)
                p = 0;
            if (p > 1)
                p = 1;
        }
        if (isInvert)
            p = 1.0 - p;
        return (min + (p * (max - min)));
    }
    static clamp(value, min, max) { return Math.max(Math.min(value, max), min); }
}
exports.Numbers = Numbers;
exports.default = Numbers;
//# sourceMappingURL=numbers.js.map