"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Perlin {
    constructor() {
        this._PERLIN_YWRAPB = 4;
        this._PERLIN_YWRAP = 1 << this._PERLIN_YWRAPB;
        this._PERLIN_ZWRAPB = 8;
        this._PERLIN_ZWRAP = 1 << this._PERLIN_ZWRAPB;
        this._PERLIN_SIZE = 4095;
        this._perlin_octaves = 4;
        this._perlin_amp_falloff = 0.5;
    }
    noise(x, y, z) {
        y = y || 0;
        z = z || 0;
        if (this._perlin == null) {
            this._perlin = new Array(this._PERLIN_SIZE + 1);
            for (let i = 0; i < this._PERLIN_SIZE + 1; i++) {
                this._perlin[i] = Math.random();
            }
        }
        if (x < 0) {
            x = -x;
        }
        if (y < 0) {
            y = -y;
        }
        if (z < 0) {
            z = -z;
        }
        let xi = Math.floor(x), yi = Math.floor(y), zi = Math.floor(z);
        let xf = x - xi;
        let yf = y - yi;
        let zf = z - zi;
        let rxf, ryf;
        let r = 0;
        let ampl = 0.5;
        let n1, n2, n3;
        for (let o = 0; o < this._perlin_octaves; o++) {
            let of = xi + (yi << this._PERLIN_YWRAPB) + (zi << this._PERLIN_ZWRAPB);
            rxf = this._scaled_cosine(xf);
            ryf = this._scaled_cosine(yf);
            n1 = this._perlin[of & this._PERLIN_SIZE];
            n1 += rxf * (this._perlin[(of + 1) & this._PERLIN_SIZE] - n1);
            n2 = this._perlin[(of + this._PERLIN_YWRAP) & this._PERLIN_SIZE];
            n2 += rxf * (this._perlin[(of + this._PERLIN_YWRAP + 1) & this._PERLIN_SIZE] - n2);
            n1 += ryf * (n2 - n1);
            of += this._PERLIN_ZWRAP;
            n2 = this._perlin[of & this._PERLIN_SIZE];
            n2 += rxf * (this._perlin[(of + 1) & this._PERLIN_SIZE] - n2);
            n3 = this._perlin[(of + this._PERLIN_YWRAP) & this._PERLIN_SIZE];
            n3 += rxf * (this._perlin[(of + this._PERLIN_YWRAP + 1) & this._PERLIN_SIZE] - n3);
            n2 += ryf * (n3 - n2);
            n1 += this._scaled_cosine(zf) * (n2 - n1);
            r += n1 * ampl;
            ampl *= this._perlin_amp_falloff;
            xi <<= 1;
            xf *= 2;
            yi <<= 1;
            yf *= 2;
            zi <<= 1;
            zf *= 2;
            if (xf >= 1.0) {
                xi++;
                xf--;
            }
            if (yf >= 1.0) {
                yi++;
                yf--;
            }
            if (zf >= 1.0) {
                zi++;
                zf--;
            }
        }
        return r;
    }
    detail(octaves, falloff) {
        if (octaves > 0)
            this._perlin_octaves = octaves;
        if (falloff > 0)
            this._perlin_amp_falloff = falloff;
    }
    ;
    seed(value) {
        let lcg = (function () {
            let m = 4294967296;
            let a = 1664525;
            let c = 1013904223;
            let seed;
            let z;
            return {
                setSeed: function (val) {
                    z = seed = (val == null ? Math.random() * m : val) >>> 0;
                },
                rand: function () {
                    z = (a * z + c) % m;
                    return z / m;
                }
            };
        }());
        lcg.setSeed(value);
        this._perlin = new Array(this._PERLIN_SIZE + 1);
        for (let i = 0; i < this._PERLIN_SIZE + 1; i++) {
            this._perlin[i] = lcg.rand();
        }
    }
    ;
    _scaled_cosine(i) {
        return 0.5 * (1.0 - Math.cos(i * Math.PI));
    }
    ;
}
exports.Perlin = Perlin;
exports.default = Perlin;
//# sourceMappingURL=perlin.js.map