export class IntersectionPoint {
    x:number;
    y:number;
    isOnLine1:boolean = false;
    isOnLine2:boolean = false;
} // End class


export class Numbers {
    static r(min:number = 0, max:number = 1, isInt:boolean = false):number { return Numbers.random(min, max, isInt); }
    static random(min:number = 0, max:number = 1, isInt:boolean = false):number {
        let r:number = (Math.random() * (max - min)) + min;
        if (isInt) r = Math.round(r);
        return r;
    }


    static randomByte():number { return Numbers.random(0, 255, true); }


    static angleToRadians(angleInDegrees:number):number { return angleInDegrees * Math.PI / 180.0; }
    static angleToDegrees(angleInRadians:number):number { return angleInRadians * 180.0 / Math.PI; }


    static angleRadians(x1:number, y1:number, x2:number, y2:number):number {
        return Math.atan2(y2 - y1, x2 - x1);
    }


    static angle(x1:number, y1:number, x2:number, y2:number):number {
        return this.angleToDegrees(this.angleRadians(x1, y1, x2, y2));
    }


    static screenToCartesian(point:number[], screenWidth:number, screenHeight:number):number[] {
        const xScreen:number = point[0];
        const yScreen:number = point[1];
        const xCartesian:number = xScreen - screenWidth / 2;
        const yCartesian:number = -yScreen + screenHeight / 2;
        return [xCartesian, yCartesian];
    }


    static cartesianToScreen(point:number[], screenWidth:number, screenHeight:number):number[] {
        const xCartesian:number = point[0];
        const yCartesian:number = point[1];
        const xScreen:number = xCartesian + screenWidth / 2;
        const yScreen:number = -yCartesian + screenHeight / 2;
        return [xScreen, yScreen];
    }


    static constrainToInterval(value:number, interval:number):number {
        return Math.round(value / interval) * interval;
    }


    static round(value:number, decimalPlaces:number):number { return this._decimalAdjust('round', value, -decimalPlaces); }
    static floor(value:number, decimalPlaces:number):number { return this._decimalAdjust('floor', value, -decimalPlaces); }
    static ceil(value:number, decimalPlaces:number):number { return this._decimalAdjust('ceil', value, -decimalPlaces); }
    /**
     * Decimal adjustment of a number.
     * See: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round
     *
     * ----- Round
     * Numbers.round(55.55, -1);   // 55.6
     * Numbers.round(55.549, -1);  // 55.5
     * Numbers.round(55, 1);       // 60
     * Numbers.round(54.9, 1);     // 50
     * Numbers.round(-55.55, -1);  // -55.5
     * Numbers.round(-55.551, -1); // -55.6
     * Numbers.round(-55, 1);      // -50
     * Numbers.round(-55.1, 1);    // -60
     * Numbers.round(1.005, -2);   // 1.01
     *
     * ----- Floor
     * Numbers.floor(55.59, -1);   // 55.5
     * Numbers.floor(59, 1);       // 50
     * Numbers.floor(-55.51, -1);  // -55.6
     * Numbers.floor(-51, 1);      // -60
     *
     * ----- Ceil
     * Numbers.ceil(55.51, -1);    // 55.6
     * Numbers.ceil(51, 1);        // 60
     * Numbers.ceil(-55.59, -1);   // -55.5
     * Numbers.ceil(-59, 1);       // -50
     *
     * @param type {string} The type of adjustment.
     * @param value {number} The number to operate upon.
     * @param exp {number} The exponent (the 10 logarithm of the adjustment base).
     * @returns {number} The adjusted value.
     */
    private static _decimalAdjust(type:string, value:number, exp:number):number {
        /// If the exp is undefined or zero...
        /// NaN is the only value in Javascript that is not equal to itself.
        if (exp == null || exp == undefined || exp != exp || exp === 0) {
            switch(type) {
                case 'round':
                    return Math.round(value);
                case 'floor':
                    return Math.floor(value);
                case 'ceil':
                    return Math.ceil(value);
            }
        }

        /// If the value is not a number or the exp is not an integer...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) return NaN;

        /// Shift
        let valueStringArray:string[] = value.toString().split('e');

        switch(type) {
            case 'round':
                value = Math.round(+(valueStringArray[0] + 'e' + (valueStringArray[1] ? (+valueStringArray[1] - exp) : -exp)));
                break;
            case 'floor':
                value = Math.floor(+(valueStringArray[0] + 'e' + (valueStringArray[1] ? (+valueStringArray[1] - exp) : -exp)));
                break;
            case 'ceil':
                value = Math.ceil(+(valueStringArray[0] + 'e' + (valueStringArray[1] ? (+valueStringArray[1] - exp) : -exp)));
                break;
        }

        /// Shift back
        valueStringArray = value.toString().split('e');
        return +(valueStringArray[0] + 'e' + (valueStringArray[1] ? (+valueStringArray[1] + exp) : exp));
    }


    /// See: http://jsfiddle.net/justin_c_rounds/Gd2S2/
    static intersection(
        line1X1:number, line1Y1:number, line1X2:number, line1Y2:number,
        line2X1:number, line2Y1:number, line2X2:number, line2Y2:number
    ):IntersectionPoint {
        /// If the lines intersect, the result has the x and y of the intersection (treating the lines as infinite)
        /// and booleans for whether line segment 1 or line segment 2 contain the point
        let denominator:number;
        let a:number;
        let b:number;
        let numerator1:number;
        let numerator2:number;
        let intersection:IntersectionPoint = new IntersectionPoint();

        denominator = ((line2Y2 - line2Y1) * (line1X2 - line1X1)) - ((line2X2 - line2X1) * (line1Y2 - line1Y1));
        if (denominator == 0) return intersection;
        a = line1Y1 - line2Y1;
        b = line1X1 - line2X1;
        numerator1 = ((line2X2 - line2X1) * a) - ((line2Y2 - line2Y1) * b);
        numerator2 = ((line1X2 - line1X1) * a) - ((line1Y2 - line1Y1) * b);
        a = numerator1 / denominator;
        b = numerator2 / denominator;

        /// If we cast these lines infinitely in both directions, they intersect here:
        intersection.x = line1X1 + (a * (line1X2 - line1X1));
        intersection.y = line1Y1 + (a * (line1Y2 - line1Y1));

        /// Note: The above should be the same as:
        /// x = line2X1 + (b * (line2X2 - line2X1));
        /// y = line2Y1 + (b * (line2Y2 - line2Y1));

        //---- If line1 is a segment and line2 is infinite, they intersect if:
        if (a > 0 && a < 1) intersection.isOnLine1 = true;
        /// if line2 is a segment and line1 is infinite, they intersect if:
        if (b > 0 && b < 1) intersection.isOnLine2 = true;
        //---- If line1 and line2 are segments, they intersect if both of the above are true
        return intersection;
    }


    static distanceBetweenPoints(pointA:number[], pointB:number[]):number {
        if (!pointA[2]) pointA[2] = 0;
        if (!pointB[2]) pointB[2] = 0;
        return Math.sqrt(Math.pow(pointA[0] - pointB[0], 2) + Math.pow(pointA[1] - pointB[1], 2) + Math.pow(pointA[2] - pointB[2], 2));
    }


    static midpoint(x1:number, y1:number, x2:number, y2:number):number[] {
        let midpoint:number[] = [];

        midpoint[0] = (x1 + x2) / 2;
        midpoint[1] = (y1 + y2) / 2;

        return midpoint;
    }


    static nearestPointOnLine(point:number[], lineStart:number[], lineEnd:number[]):number[] {
        if (!point[2]) point[2] = 0;
        if (!lineStart[2]) lineStart[2] = 0;
        if (!lineEnd[2]) lineEnd[2] = 0;

        const lineLength: number = this.distanceBetweenPoints(lineStart, lineEnd);
        let pointNearestOnLine:number[];
        let nearestPoint:number[];

        if (lineLength === 0) {
            pointNearestOnLine = [lineStart[0], lineStart[1], lineStart[2]];
            return [pointNearestOnLine[0], pointNearestOnLine[1], pointNearestOnLine[2], , 0];
        }

        /// p will be a number between 0 and 1 that represents the relative position
        /// of the point along the line, with 0 being the start and 1 being the end.
        const p = (
            ((point[0] - lineStart[0]) * (lineEnd[0] - lineStart[0])) +
            ((point[1] - lineStart[1]) * (lineEnd[1] - lineStart[1])) +
            ((point[2] - lineStart[2]) * (lineEnd[2] - lineStart[2]))
        ) / lineLength;

        if (p < 0) {
            /// The point is before the start of the line.
            pointNearestOnLine = [lineStart[0], lineStart[1], lineStart[2]];
            nearestPoint = [pointNearestOnLine[0], pointNearestOnLine[1], pointNearestOnLine[2], p];
            return nearestPoint;
        }

        if (p > 1) {
            /// The point is beyond the end of the line.
            pointNearestOnLine = [lineEnd[0], lineEnd[1], lineEnd[2]];
            nearestPoint = [pointNearestOnLine[0], pointNearestOnLine[1], pointNearestOnLine[2], p];
            return nearestPoint;
        }

        /// The nearest point is somewhere along the length of the line.
        pointNearestOnLine = this.pointOnLine(p, lineStart, lineEnd);

        nearestPoint = [pointNearestOnLine[0], pointNearestOnLine[1], pointNearestOnLine[2], p];
        return nearestPoint;
    }


    static pointOnLine(p:number, lineStart:number[], lineEnd:number[]):number[] {
        if (!lineStart[2]) lineStart[2] = 0;
        if (!lineEnd[2]) lineEnd[2] = 0;
        const xNew:number = lineStart[0] + (p * (lineEnd[0] - lineStart[0]));
        const yNew:number = lineStart[1] + (p * (lineEnd[1] - lineStart[1]));
        const zNew:number = lineStart[2] + (p * (lineEnd[2] - lineStart[2]));
        return [xNew, yNew, zNew];
    }


    static pointOnLineScreen(p:number, lineStart:number[], lineEnd:number[], screenWidth:number, screenHeight:number):number[] {
        lineStart = this.screenToCartesian(lineStart, screenWidth, screenHeight);
        lineEnd = this.screenToCartesian(lineEnd, screenWidth, screenHeight);
        const pointOnLine:number[] = this.pointOnLine(p, lineStart, lineEnd);
        return this.cartesianToScreen(pointOnLine, screenWidth, screenHeight);
    }


    static distanceToLine(point:number[], lineStart:number[], lineEnd:number[]):number {
        if (!point[2]) point[2] = 0;
        if (!lineStart[2]) lineStart[2] = 0;
        if (!lineEnd[2]) lineEnd[2] = 0;
        const pointNearestOnLine:number[] = this.nearestPointOnLine(point, lineStart, lineEnd);
        const distanceToNearestPoint:number = this.distanceBetweenPoints(point, pointNearestOnLine);
        return Math.pow(distanceToNearestPoint, 2);
    }


    static randomPointWithinCircle(x:number, y:number, radius:number):number[] {
        const pt_angle = Math.random() * 2 * Math.PI;
        const pt_radius_sq = Math.random() * radius * radius;
        const pt_x = (Math.sqrt(pt_radius_sq) * Math.cos(pt_angle)) + x;
        const pt_y = (Math.sqrt(pt_radius_sq) * Math.sin(pt_angle)) + y;
        return [pt_x, pt_y];
    }


    static isPointInEllipse(
        x:number,
        y:number,
        ellipseCenterX:number,
        ellipseCenterY:number,
        radiusX:number,
        radiusY:number,
        rotation:number
    ):boolean {
        rotation = rotation || 0;
        let cos = Math.cos(rotation);
        let sin = Math.sin(rotation);
        let dx  = (x - ellipseCenterX);
        let dy  = (y - ellipseCenterY);
        let tdx = cos * dx + sin * dy;
        let tdy = sin * dx - cos * dy;

        return (tdx * tdx) / (radiusX * radiusX) + (tdy * tdy) / (radiusY * radiusY) <= 1;
    }


    static pointOnEllipse(
        angleAlongCircumference:number,
        ellipseRadiusX:number,
        ellipseRadiusY:number,
        ellipseCenterX:number = 0,
        ellipseCenterY:number = 0
    ):number[] {
        /// We need a number between 0 and 1 for percent.
        const percent:number = angleAlongCircumference > 1 ? angleAlongCircumference / 360 : angleAlongCircumference;
        const x:number = (Math.cos(2 * Math.PI * percent) * ellipseRadiusX) + ellipseCenterX;
        const y:number = (Math.sin(2 * Math.PI * percent) * ellipseRadiusY) + ellipseCenterY;
        return [x, y];
    }


    static rotate(x:number, y:number, angle:number, originX:number = 0, originY:number = 0):number[] {
        angle = this.angleToRadians(angle);
        return this.rotateRadians(x, y, angle, originX, originY);
    }


    static rotateRadians(x:number, y:number, angle:number, originX:number = 0, originY:number = 0):number[] {
        return this.rotateZ(x, y, 0, angle, originX, originY, 0);
    }


    static rotateX(x:number, y:number, z:number, angle:number, originX:number = 0, originY:number = 0, originZ:number = 0):number[] {
        angle = this.angleToRadians(angle);
        return this.rotateXRadians(x, y, z, angle, originX, originY, originZ);
    }

    static rotateXRadians(x:number, y:number, z:number, angle:number, originX:number = 0, originY:number = 0, originZ:number = 0):number[] {
        let cos:number = Math.cos(angle);
        let sin:number = Math.sin(angle);

        let rotatedY:number = (cos * (y - originY)) - (sin * (z - originZ)) + originY;
        let rotatedZ:number = (cos * (z - originZ)) + (sin * (y - originY)) + originZ;

        return [x, rotatedY, rotatedZ];
    }

    static rotateY(x:number, y:number, z:number, angle:number, originX:number = 0, originY:number = 0, originZ:number = 0):number[] {
        angle = this.angleToRadians(angle);
        return this.rotateYRadians(x, y, z, angle, originX, originY, originZ);
    }

    static rotateYRadians(x:number, y:number, z:number, angle:number, originX:number = 0, originY:number = 0, originZ:number = 0):number[] {
        let cos:number = Math.cos(angle);
        let sin:number = Math.sin(angle);

        let rotatedX:number = (cos * (x - originX)) - (sin * (z - originZ)) + originX;
        let rotatedZ:number = (cos * (z - originZ)) + (sin * (x - originX)) + originZ;

        return [rotatedX, y, rotatedZ];
    }

    static rotateZ(x:number, y:number, z:number, angle:number, originX:number = 0, originY:number = 0, originZ:number = 0):number[] {
        angle = this.angleToRadians(angle);
        return this.rotateZRadians(x, y, z, angle, originX, originY, originZ);
    }

    static rotateZRadians(x:number, y:number, z:number, angle:number, originX:number = 0, originY:number = 0, originZ:number = 0):number[] {
        let cos:number = Math.cos(angle);
        let sin:number = Math.sin(angle);

        let rotatedX:number = (cos * (x - originX)) - (sin * (y - originY)) + originX;
        let rotatedY:number = (cos * (y - originY)) + (sin * (x - originX)) + originY;

        return [rotatedX, rotatedY, z];
    }


    static getIndices(indexStart:number, indexEnd:number, isShuffle:boolean):number[] {
        let currentIndex:number = indexEnd;
        let temporaryValue:number;
        let randomIndex:number;
        const indexArray:number[] = [];

        for (let i = indexStart; i <= indexEnd; i++) {
            indexArray.push(i);
        }

        if (!isShuffle) return indexArray;

        /// While there remain elements to shuffle...
        while (currentIndex !== indexStart) {
            /// Pick a remaining element...
            randomIndex = <number>Math.floor(Math.random() * currentIndex);
            currentIndex--;

            /// And swap it with the current element.
            temporaryValue = indexArray[currentIndex];
            indexArray[currentIndex] = indexArray[randomIndex];
            indexArray[randomIndex] = temporaryValue;
        }

        return indexArray;
    }


    /**
     * Returns the relative position of the input value
     * within the min/max range as a value between 0.0 and 1.0.
     */
    static normalize(
        value:number,
        min:number,
        max:number,
        isConstrain:boolean = true,
        isInvert:boolean = false
    ):number {
        let p:number = (value - min) / (max - min);

        if (isConstrain) {
            if (p < 0) p = 0;
            if (p > 1) p = 1;
        }

        if (isInvert) p = 1.0 - p;
        return p;
    }


    /**
     * Accepts a value between 0.0 and 1.0 and returns the input value at the corresponding
     * progress point within the min/max input range.
     */
    static unnormalize(
        valueNormalized:number,
        min:number,
        max:number,
        isConstrain:boolean = true,
        isInvert:boolean = false
    ):number {
        let p:number = valueNormalized;

        if (isConstrain) {
            if (p < 0) p = 0;
            if (p > 1) p = 1;
        }

        if (isInvert) p = 1.0 - p;
        return (min + (p * (max - min)));
    }


    static clamp(value:number, min:number, max:number) { return Math.max(Math.min(value, max), min); }
} // End class


export default Numbers;
