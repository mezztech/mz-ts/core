"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const perlin_1 = require("./src/perlin");
exports.Perlin = perlin_1.default;
const numbers_1 = require("./src/numbers");
exports.Numbers = numbers_1.default;
exports.default = numbers_1.default;
//# sourceMappingURL=index.js.map