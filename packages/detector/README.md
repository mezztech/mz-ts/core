# detector

Use to determine context and capabilities of run-time environment.

## Install

`sudo npm install --save @mezzy/detector`

## Usage

```
import Detector from @mezzy/detector

let isSafariMobile:boolean = Detector.isSafariMobile;
```
