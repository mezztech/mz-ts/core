"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class RegularExpressions {
    static escape(value) {
        return value.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    }
    static getGroupCount(value) {
        const regExp = new RegExp('|' + value);
        return regExp.exec('').length - 1;
    }
    static union(value) {
        const source = value.map((regExpString) => { return "(?:" + regExpString + ")"; }).join('|');
        return "(?:" + source + ")";
    }
}
exports.RegularExpressions = RegularExpressions;
exports.default = RegularExpressions;
//# sourceMappingURL=regularExpressions.js.map