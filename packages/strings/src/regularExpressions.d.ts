export declare class RegularExpressions {
    static escape(value: string): string;
    static getGroupCount(value: string): number;
    static union(value: string[]): string;
}
export default RegularExpressions;
