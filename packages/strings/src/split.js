"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Split = (function () {
    function Split(e, t) {
        if (typeof e === 'string' && (e = Split.selector(e)), !e)
            throw "Cannot split a null element.";
        this._elements = p(e) ? a(e) : [e];
        this._chars = [];
        this._words = [];
        this._lines = [];
        this._originals = [];
        this._vars = t || {};
        this.split(t);
    }
    Object.defineProperty(Split, "version", {
        get: function () { return '0.5.8'; },
        enumerable: true,
        configurable: true
    });
    Split.selector = function (e) {
        if (e.$ || e.JQuery) {
        }
        e.$ || e.jQuery || (function (t) {
            var i = e.$ || e.jQuery;
            return i ? (T.selector = i, i(t)) : "undefined" == typeof document ? t : document.querySelectorAll ? document.querySelectorAll(t) : document.getElementById("#" === t.charAt(0) ? t.substr(1) : t);
        });
    };
    Split.prototype.split = function (t) { };
    return Split;
}());
exports.Split = Split;
exports.default = Split;
//# sourceMappingURL=split.js.map