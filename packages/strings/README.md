# strings

String utilities.

## Install

`sudo npm install --save @mezzy/strings`

## Usage

```
import Strings from @mezzy/strings

Strings.trimWhitespace(inputString);
```
