"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const hiResTimer_1 = require("./src/hiResTimer");
exports.HiResTimer = hiResTimer_1.default;
const hiResTimerCounterBrowser_1 = require("./src/hiResTimerCounterBrowser");
exports.HiResTimerCounterBrowser = hiResTimerCounterBrowser_1.default;
const hiResTimerEnums_1 = require("./src/hiResTimerEnums");
exports.TimerEventInterval = hiResTimerEnums_1.TimerEventInterval;
const timer_1 = require("./src/timer");
exports.Timer = timer_1.default;
const timespan_1 = require("./src/timespan");
exports.TimeSpan = timespan_1.default;
exports.default = timer_1.default;
//# sourceMappingURL=index.js.map