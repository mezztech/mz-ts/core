export interface IHiResTimerCounter {
    frequency: number;
    count: number;
    newCounter(): IHiResTimerCounter;
}
export default IHiResTimerCounter;
