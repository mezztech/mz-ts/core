"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TimerEventInterval;
(function (TimerEventInterval) {
    TimerEventInterval[TimerEventInterval["NONE"] = 0] = "NONE";
    TimerEventInterval[TimerEventInterval["MILLISECONDS"] = 1] = "MILLISECONDS";
    TimerEventInterval[TimerEventInterval["CENTISECONDS"] = 2] = "CENTISECONDS";
    TimerEventInterval[TimerEventInterval["DECISECONDS"] = 3] = "DECISECONDS";
    TimerEventInterval[TimerEventInterval["SECONDS"] = 4] = "SECONDS";
    TimerEventInterval[TimerEventInterval["MINUTES"] = 5] = "MINUTES";
    TimerEventInterval[TimerEventInterval["HOURS"] = 6] = "HOURS";
})(TimerEventInterval = exports.TimerEventInterval || (exports.TimerEventInterval = {}));
//# sourceMappingURL=hiResTimerEnums.js.map