export enum TimerEventInterval {
    NONE,
    MILLISECONDS,
    CENTISECONDS,
    DECISECONDS,
    SECONDS,
    MINUTES,
    HOURS
} // End enum
