import HiResTimer from './src/hiResTimer';
import HiResTimerCounterBrowser from './src/hiResTimerCounterBrowser';
import IHiResTimerCounter from './src/iHiResTimerCounter';
import { TimerEventInterval } from './src/hiResTimerEnums';
import Timer from './src/timer';
import TimeSpan from './src/timespan';
export { HiResTimer, HiResTimerCounterBrowser, IHiResTimerCounter, TimerEventInterval, Timer, TimeSpan };
export default Timer;
