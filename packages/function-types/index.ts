/**
 * Function signature for comparing
 * < 0 means a is smaller
 * = 0 means they are equal
 * > 0 means a is larger
 */
export interface ICompareFunction<T> { (a:T, b:T):number; }


/**
 * Function signature for checking equality
 */
export interface IEqualsFunction<T> { (a:T, b:T):boolean; }


/**
 * Function signature for Iterations. Return false to break from loop
 */
export interface ILoopFunction<T> { (a:T):boolean; }


export interface IFilterFunction<T> { (a:T):boolean; }
