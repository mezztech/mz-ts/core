# function-types

General function interfaces for commonly used function signatures.

## Install

sudo npm install --save @mezzy/function-types

## Usage

```
import {IEqualsFunction, ICompareFunction, ILoopFunction} from '@mezzy/function-types';
```
