# detector-node-js

Use to determine context and capabilities of run-time environment for Node.js apps. This library will generate console errors if run in a browser outside of a Node.js environment.

## Install

`sudo npm install --save @mezzy/detector`

## Usage

```
import DetectorNodeJs from @mezzy/detector-node-js

let isModuleInstalled:boolean = DetectorNodeJs.isNodeModuleInstalled('@mezzy/strings');
```
