# debug

Debugging utilities.

## Install

sudo npm install --save @mezzy/debug

## Usage

```
import Debug from '@mezzy/debug';

Debug.assert(
     is.notEmpty(myVariable),
     'The value of myVariable is %MY%. The value of otherVariable is %OTHER%.',
     'AwesomeClass.doSomethingCool(...)', {"MY": myVariable, "OTHER": otherVariable});
```
