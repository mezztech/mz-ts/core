export declare class Tests {
    test01(testValue: any): void;
    test02(testValue: any): void;
    test03(testValue: any): void;
    test04(testValue: any): void;
    test05(testValue: any): void;
    asyncTest(testValue: string): Promise<void>;
}
