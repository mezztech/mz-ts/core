"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const alsatian_1 = require("alsatian");
const index_1 = require("../index");
index_1.Debug.setCategoryMode('categoryTest', true);
let Tests = class Tests {
    test01(testValue) {
        index_1.Debug.logger.isTrace = false;
        index_1.Debug.log(testValue);
        index_1.Debug.log(testValue, 'this is a tag');
        index_1.Debug.warn(testValue, 'Data.p_processAttributeNumber');
        index_1.Debug.error(testValue, 'this a tag that is particularly long and is bound to exceed the maximum tag length constraint');
        index_1.Debug.assert(testValue === 'hello', `${testValue} is not equal to 'hello'`, 'testingTag');
        index_1.Debug.info(testValue, 'just for your information');
        index_1.Debug.test(testValue, 'this is a tag');
        index_1.Debug.logger.custom(testValue);
        index_1.Debug.logger.custom(testValue, 'Hey there!', index_1.LogColor.BLUE_BRIGHT, 'how about this tag?');
    }
    test02(testValue) {
        index_1.Debug.logger.isTrace = true;
        index_1.Debug.warn(testValue, 'test tag');
    }
    test03(testValue) {
        index_1.Debug.logger.isTrace = false;
        index_1.Debug.data(testValue);
    }
    test04(testValue) {
        let logMessage = index_1.Debug.log(testValue, 'this is a tag');
        index_1.Debug.info(`message line count: ${logMessage.lineCount}`);
        index_1.Debug.info(`message label: ${logMessage.label}`);
        index_1.Debug.info(`message type: ${logMessage.type.toString()}`);
        index_1.Debug.info(`message tag: ${logMessage.tag}`);
        index_1.Debug.info(`message category: ${logMessage.category}`);
    }
    test05(testValue) {
        let logMessage = index_1.Debug.log(testValue, 'this is a tag', 'categoryTest');
        if (index_1.Debug.logger.isCategoryOn('categoryTest')) {
            index_1.Debug.logger.console.cursor.up(logMessage.lineCount + 1);
            index_1.Debug.logger.console.erase(index_1.LogRegion.DOWN);
            index_1.Debug.test(logMessage.message, logMessage.tag, logMessage.category);
        }
    }
    asyncTest(testValue) {
        return __awaiter(this, void 0, void 0, function* () {
            let logMessage = index_1.Debug.log(testValue, 'this is a tag', 'categoryTest');
            const promise = new Promise((resolve, reject) => {
                setTimeout(() => {
                    logMessage.startSpinner();
                    setTimeout(() => {
                        logMessage.stopSpinner();
                        resolve();
                    }, 8000);
                }, 1000);
            });
            const result = yield promise;
        });
    }
};
__decorate([
    alsatian_1.TestCase(null),
    alsatian_1.TestCase(undefined),
    alsatian_1.TestCase(''),
    alsatian_1.TestCase(NaN),
    alsatian_1.TestCase(0),
    alsatian_1.TestCase(1),
    alsatian_1.TestCase(-1),
    alsatian_1.TestCase('hello'),
    alsatian_1.TestCase('Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?'),
    alsatian_1.TestCase(false),
    alsatian_1.Test('logging')
], Tests.prototype, "test01", null);
__decorate([
    alsatian_1.TestCase('This is a traced warning.'),
    alsatian_1.Test('log tracing')
], Tests.prototype, "test02", null);
__decorate([
    alsatian_1.TestCase({ property01: "value01", property02: "value02", property03: "value03", property04: "value04", property05: "value05", property06: "value06", property07: "value07", property08: "value08" }),
    alsatian_1.Test('objects and data')
], Tests.prototype, "test03", null);
__decorate([
    alsatian_1.TestCase('Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?'),
    alsatian_1.Test('LogMessage return value')
], Tests.prototype, "test04", null);
__decorate([
    alsatian_1.TestCase('Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?'),
    alsatian_1.Test('cursor manipulation')
], Tests.prototype, "test05", null);
__decorate([
    alsatian_1.TestCase('Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?'),
    alsatian_1.AsyncTest("spinner"),
    alsatian_1.Timeout(10000)
], Tests.prototype, "asyncTest", null);
Tests = __decorate([
    alsatian_1.TestFixture('Debug')
], Tests);
exports.Tests = Tests;
//# sourceMappingURL=index.test.js.map