"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LogType;
(function (LogType) {
    LogType[LogType["ASSERT"] = 0] = "ASSERT";
    LogType[LogType["CUSTOM"] = 1] = "CUSTOM";
    LogType[LogType["DATA"] = 2] = "DATA";
    LogType[LogType["ERROR"] = 3] = "ERROR";
    LogType[LogType["INFO"] = 4] = "INFO";
    LogType[LogType["WARNING"] = 5] = "WARNING";
    LogType[LogType["LOG"] = 6] = "LOG";
    LogType[LogType["TEST"] = 7] = "TEST";
})(LogType = exports.LogType || (exports.LogType = {}));
var LogColor;
(function (LogColor) {
    LogColor[LogColor["BLACK"] = 0] = "BLACK";
    LogColor[LogColor["BLACK_BRIGHT"] = 1] = "BLACK_BRIGHT";
    LogColor[LogColor["BLUE"] = 2] = "BLUE";
    LogColor[LogColor["BLUE_BRIGHT"] = 3] = "BLUE_BRIGHT";
    LogColor[LogColor["CYAN"] = 4] = "CYAN";
    LogColor[LogColor["CYAN_BRIGHT"] = 5] = "CYAN_BRIGHT";
    LogColor[LogColor["GRAY"] = 6] = "GRAY";
    LogColor[LogColor["GREEN"] = 7] = "GREEN";
    LogColor[LogColor["GREEN_BRIGHT"] = 8] = "GREEN_BRIGHT";
    LogColor[LogColor["MAGENTA"] = 9] = "MAGENTA";
    LogColor[LogColor["MAGENTA_BRIGHT"] = 10] = "MAGENTA_BRIGHT";
    LogColor[LogColor["ORANGE"] = 11] = "ORANGE";
    LogColor[LogColor["RED"] = 12] = "RED";
    LogColor[LogColor["RED_BRIGHT"] = 13] = "RED_BRIGHT";
    LogColor[LogColor["YELLOW"] = 14] = "YELLOW";
    LogColor[LogColor["YELLOW_BRIGHT"] = 15] = "YELLOW_BRIGHT";
})(LogColor = exports.LogColor || (exports.LogColor = {}));
var LogRegion;
(function (LogRegion) {
    LogRegion[LogRegion["LINE"] = 0] = "LINE";
    LogRegion[LogRegion["LINE_LEFT"] = 1] = "LINE_LEFT";
    LogRegion[LogRegion["LINE_RIGHT"] = 2] = "LINE_RIGHT";
    LogRegion[LogRegion["DOWN"] = 3] = "DOWN";
    LogRegion[LogRegion["SCREEN"] = 4] = "SCREEN";
})(LogRegion = exports.LogRegion || (exports.LogRegion = {}));
//# sourceMappingURL=logEnums.js.map