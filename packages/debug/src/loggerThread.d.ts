import { LogColor } from './logEnums';
import Logger from './logger';
import LogMessage from './logMessage';
export declare class LoggerThread extends Logger {
    constructor();
    custom(message: any, label?: string, labelColor?: LogColor, tag?: string, category?: string, spinnerCharacter?: string): LogMessage;
}
export default LoggerThread;
