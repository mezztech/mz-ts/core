export enum LogType {
    ASSERT,
    CUSTOM,
    DATA,
    ERROR,
    INFO,
    WARNING,
    LOG,
    TEST
} // End enum


export enum LogColor {
    BLACK,
    BLACK_BRIGHT,
    BLUE,
    BLUE_BRIGHT,
    CYAN,
    CYAN_BRIGHT,
    GRAY,
    GREEN,
    GREEN_BRIGHT,
    MAGENTA,
    MAGENTA_BRIGHT,
    ORANGE,
    RED,
    RED_BRIGHT,
    YELLOW,
    YELLOW_BRIGHT
} // End enum


export enum LogRegion {
    LINE,
    LINE_LEFT,
    LINE_RIGHT,
    DOWN,
    SCREEN
} // End enum
