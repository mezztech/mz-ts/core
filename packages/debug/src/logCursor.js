"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class LogCursor {
    constructor(console) {
        this._console = console;
    }
    setLine(value) { return this.setPosition(undefined, value); }
    setColumn(value) { return this.setPosition(value, undefined); }
    setPosition(x, y) {
        const stream = this._console.stream;
        if (stream === null || stream === undefined)
            return this;
        if (typeof x !== 'number' && typeof y !== 'number')
            return this;
        const escape = '\x1b';
        function CSI(strings, ...args) {
            let ret = `${escape}[`;
            for (let n = 0; n < strings.length; n++) {
                ret += strings[n];
                if (n < args.length)
                    ret += args[n];
            }
            return ret;
        }
        if (typeof y !== 'number')
            stream.write(CSI `${x + 1}G`);
        else
            stream.write(CSI `${y + 1};${x + 1}H`);
        return this;
    }
    move(x, y) {
        const stream = this._console.stream;
        if (stream === null || stream === undefined)
            return this;
        const escape = '\x1b';
        function CSI(strings, ...args) {
            let ret = `${escape}[`;
            for (let n = 0; n < strings.length; n++) {
                ret += strings[n];
                if (n < args.length)
                    ret += args[n];
            }
            return ret;
        }
        if (x < 0)
            stream.write(CSI `${-x}D`);
        else if (x > 0)
            stream.write(CSI `${x}C`);
        if (y < 0)
            stream.write(CSI `${-y}A`);
        else if (y > 0)
            stream.write(CSI `${y}B`);
        return this;
    }
    up(value = 1) { return this.move(0, -Math.abs(value)); }
    down(value = 1) { return this.move(0, Math.abs(value)); }
    left(value = 1) { return this.move(-Math.abs(value), 0); }
    right(value = 1) { return this.move(Math.abs(value), 0); }
    hide() {
        this._console.stream.write(this._encode('[?25l'));
        return this;
    }
    show() {
        this._console.stream.write(this._encode('[?25h'));
        return this;
    }
    _encode(value) {
        return new Buffer([0x1b].concat(value.split('').map((value) => value.charCodeAt(0))));
    }
    ;
}
exports.LogCursor = LogCursor;
exports.default = LogCursor;
//# sourceMappingURL=logCursor.js.map