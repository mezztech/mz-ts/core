export enum FileType {
    FILE_ENTRY,
    FILE_ENTRY_DIRECTORY,
    FILE_HTML,
    FILE_IMAGE,
    FILE_IMAGE_JPEG,
    FILE_IMAGE_PNG,
    FILE_JAVASCRIPT,
    FILE_JSON,
    FILE_SVG,
    FILE_TEXT,
    FILE_VIDEO,
    FILE_XML,
    FILE_ZIP
}


export enum FileCreateMode {
    ALLOW_DUPLICATES,
    REPLACE_EXISTING,
    RETURN_EXISTING
} // End enum


export enum FileErrorCode {
    NOT_SUPPORTED_ERR = 0,
    NOT_FOUND_ERR = 1,
    SECURITY_ERR = 2,
    NOT_READABLE_ERR = 4,
    ENCODING_ERR = 5,
    NO_MODIFICATION_ALLOWED_ERR = 6,
    INVALID_STATE_ERR = 7,
    INVALID_MODIFICATION_ERR = 9,
    QUOTA_EXCEEDED_ERR = 10,
    TYPE_MISMATCH_ERR = 11,
    PATH_EXISTS_ERR = 12
}
