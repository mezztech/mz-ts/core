import { IPromise } from '@mezzy/result';
import IFileDirectory from './iFileDirectory';
export interface IEntry {
    name: string;
    url: string;
    isDirectory: boolean;
    isFile: boolean;
    getParent(): IPromise<IFileDirectory>;
    delete(): IPromise<boolean>;
}
export default IEntry;
