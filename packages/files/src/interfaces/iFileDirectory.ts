import { IPromise } from '@mezzy/result';
import { List } from '@mezzy/collections';
import IEntry from './iEntry';
import IFile from './iFile';
import { FileCreateMode } from '../fileEnums';


export interface IFileDirectory extends IEntry {
    getDirectory(path: string): IPromise<IFileDirectory>;
    createDirectory(path: string, mode?: FileCreateMode):IPromise<IFileDirectory>;


    getFile(path: string):IPromise<IFile>;
    createFile(path: string, data: any, mode?: FileCreateMode, ...options: any[]):IPromise<IFile>;


    getDirectoryList():IPromise<List<IEntry>>;


    clear():IPromise<IFileDirectory>;
} // End interface


export default IFileDirectory;
