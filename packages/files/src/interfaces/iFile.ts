import { IPromise } from '@mezzy/result';
import { DataType } from '@mezzy/data';
import IEntry from './iEntry';


export interface IFile extends IEntry {
    getMimeType():IPromise<string>;
    getDataType():IPromise<DataType | string>
    getSizeInBytes():IPromise<number>;
    getBlob():IPromise<Blob>;
    getDataUrl():IPromise<string>;
    readFile():IPromise<string>;
    writeToFile(data:(string | Blob), dataType?:(DataType | string), isAppend?:boolean):IPromise<boolean>;
} // End of interface


export default IFile;
