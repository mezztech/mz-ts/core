"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FileType;
(function (FileType) {
    FileType[FileType["FILE_ENTRY"] = 0] = "FILE_ENTRY";
    FileType[FileType["FILE_ENTRY_DIRECTORY"] = 1] = "FILE_ENTRY_DIRECTORY";
    FileType[FileType["FILE_HTML"] = 2] = "FILE_HTML";
    FileType[FileType["FILE_IMAGE"] = 3] = "FILE_IMAGE";
    FileType[FileType["FILE_IMAGE_JPEG"] = 4] = "FILE_IMAGE_JPEG";
    FileType[FileType["FILE_IMAGE_PNG"] = 5] = "FILE_IMAGE_PNG";
    FileType[FileType["FILE_JAVASCRIPT"] = 6] = "FILE_JAVASCRIPT";
    FileType[FileType["FILE_JSON"] = 7] = "FILE_JSON";
    FileType[FileType["FILE_SVG"] = 8] = "FILE_SVG";
    FileType[FileType["FILE_TEXT"] = 9] = "FILE_TEXT";
    FileType[FileType["FILE_VIDEO"] = 10] = "FILE_VIDEO";
    FileType[FileType["FILE_XML"] = 11] = "FILE_XML";
    FileType[FileType["FILE_ZIP"] = 12] = "FILE_ZIP";
})(FileType = exports.FileType || (exports.FileType = {}));
var FileCreateMode;
(function (FileCreateMode) {
    FileCreateMode[FileCreateMode["ALLOW_DUPLICATES"] = 0] = "ALLOW_DUPLICATES";
    FileCreateMode[FileCreateMode["REPLACE_EXISTING"] = 1] = "REPLACE_EXISTING";
    FileCreateMode[FileCreateMode["RETURN_EXISTING"] = 2] = "RETURN_EXISTING";
})(FileCreateMode = exports.FileCreateMode || (exports.FileCreateMode = {}));
var FileErrorCode;
(function (FileErrorCode) {
    FileErrorCode[FileErrorCode["NOT_SUPPORTED_ERR"] = 0] = "NOT_SUPPORTED_ERR";
    FileErrorCode[FileErrorCode["NOT_FOUND_ERR"] = 1] = "NOT_FOUND_ERR";
    FileErrorCode[FileErrorCode["SECURITY_ERR"] = 2] = "SECURITY_ERR";
    FileErrorCode[FileErrorCode["NOT_READABLE_ERR"] = 4] = "NOT_READABLE_ERR";
    FileErrorCode[FileErrorCode["ENCODING_ERR"] = 5] = "ENCODING_ERR";
    FileErrorCode[FileErrorCode["NO_MODIFICATION_ALLOWED_ERR"] = 6] = "NO_MODIFICATION_ALLOWED_ERR";
    FileErrorCode[FileErrorCode["INVALID_STATE_ERR"] = 7] = "INVALID_STATE_ERR";
    FileErrorCode[FileErrorCode["INVALID_MODIFICATION_ERR"] = 9] = "INVALID_MODIFICATION_ERR";
    FileErrorCode[FileErrorCode["QUOTA_EXCEEDED_ERR"] = 10] = "QUOTA_EXCEEDED_ERR";
    FileErrorCode[FileErrorCode["TYPE_MISMATCH_ERR"] = 11] = "TYPE_MISMATCH_ERR";
    FileErrorCode[FileErrorCode["PATH_EXISTS_ERR"] = 12] = "PATH_EXISTS_ERR";
})(FileErrorCode = exports.FileErrorCode || (exports.FileErrorCode = {}));
//# sourceMappingURL=fileEnums.js.map