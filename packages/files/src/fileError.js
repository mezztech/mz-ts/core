"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fileEnums_1 = require("./fileEnums");
class FileError extends Error {
    constructor(code = fileEnums_1.FileErrorCode.NOT_SUPPORTED_ERR) {
        super();
        this.p_code = code;
    }
    get code() { return this.p_code; }
}
exports.FileError = FileError;
exports.default = FileError;
//# sourceMappingURL=fileError.js.map