import Signal from './signal';


export class SignalMonitor<TValue> {
    constructor(isListenOnce:boolean = true) {
        this._isListenOnce = isListenOnce;
        this.allSignalsDispatched = new Signal<TValue[]>(true);
        this._isEnabled = false;
        this._values = [];

        this._signals = [];
        this._signalsPending = [];
    }


    private _isListenOnce:boolean;
    private _isEnabled:boolean;
    private _values:TValue[];


    get signals():Signal<TValue>[]{ return this._signals; }
    private readonly _signals:Signal<TValue>[];


    get signalsPending():Signal<TValue>[] { return this._signalsPending; }
    private readonly _signalsPending:Signal<TValue>[];


    allSignalsDispatched:Signal<TValue[]>;


    add(signal:Signal<TValue>):void {
        if (!signal || !(signal instanceof Signal)) return;
        this._signals.push(signal);
    }


    remove(signal:Signal<TValue>):Signal<TValue> {
        try {
            this._signals.splice(this._signals.indexOf(signal), 1);
            this._signalsPending.splice(this._signals.indexOf(signal), 1);
        } catch(e) {
            console.log('Unable to remove Signal from SignalMonitor');
        }

        return signal;
    }


    start():void {
        this._isEnabled = true;
        this._values = [];

        this._signalsPending.splice(0);
        if (this._signals.length < 1) return;

        const length:number = this._signals.length;
        for (let i = 0; i < length; i++) {
            let signal:Signal<TValue> = this._signals[i];

            this._signalsPending.push(signal);

            signal.listenOnce((value:TValue) => {
                if (this._signalsPending.length > 0) {
                    this._values.push(value);
                    this._signalsPending.splice(this._signalsPending.indexOf(signal), 1);
                    if (this._signalsPending.length < 1) {
                        if (this._isEnabled) {
                            this.allSignalsDispatched.dispatch(this._values);
                        }

                        if (!this._isListenOnce) this.start();
                    }
                }
            }, this);
        }
    }


    clear():void {
        this._isEnabled = false;
        this._signals.splice(0);
        this._signalsPending.splice(0);
        this._values = [];
    }
} // End Class


export default SignalMonitor;
