import Signal from './signal';
export declare class SignalMonitor<TValue> {
    constructor(isListenOnce?: boolean);
    private _isListenOnce;
    private _isEnabled;
    private _values;
    readonly signals: Signal<TValue>[];
    private readonly _signals;
    readonly signalsPending: Signal<TValue>[];
    private readonly _signalsPending;
    allSignalsDispatched: Signal<TValue[]>;
    add(signal: Signal<TValue>): void;
    remove(signal: Signal<TValue>): Signal<TValue>;
    start(): void;
    clear(): void;
}
export default SignalMonitor;
