/**
 *	@desc   	An object that represents a binding between a Signal and a listener function.
 *               Released under the MIT license
 *				http://millermedeiros.github.com/js-signals/
 *
 *	@version	1.0 - 7th March 2013
 *
 *	@author 	Richard Davey, TypeScript conversion
 *	@author		Miller Medeiros, JS Signals
 *	@author		Robert Penner, AS Signals
 *
 *	@url		http://www.kiwijs.org
 *
 */
export interface ISignalBinding<T> {
    /**
     * Context on which listener will be executed (object that should represent the `this` variable inside listener function).
     */
    context:any;


    /**
     * Listener priority
     */
    priority:number;


    /**
     * If binding is active and should be executed.
     */
    isActive:boolean;


    /**
     * Call listener passing arbitrary parameters.
     * <p>If binding was added using `Signal.listenOnce()` it will be automatically deleted from signal dispatch queue,
     * this method is used internally for the signal dispatch.</p>
     * @param {T} [param] Parameter that should be passed to the listener
     * @return {*} Value returned by the listener.
     */
    execute(param:T):any;


    /**
     * Detach binding from signal.
     * - alias to: mySignal.delete(myBinding.getListener());
     * @return {(value:T) => void|null} Handler function bound to the signal or `null` if binding was previously detached.
     */
    detach():(value:T) => void;

    /**
     * @return {boolean} Is the binding still bound to the signal with a listener?
     */
    isBound():boolean;


    /**
     * @return {boolean} Will SignalBinding be executed only once?
     */
    isOnce():boolean;


    /**
     * @return {(value:T) => void} Handler function bound to the signal.
     */
    readonly listener:(value:T) => void;


    /**
     * Delete instance properties
     * @private
     */
    destroy():void;
    /**
     * @return {string} String representation of the object.
     */
    toString():string;
} // End interface


export default ISignalBinding;
