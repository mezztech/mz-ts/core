# signals

Use to dispatch and listen for event signals.

## Install

`sudo npm install --save @mezzy/signals`

## Usage

```
import Signal from '@mezzy/signals';

/** 
* Pass true to the constructor, if you want new listeners
* to be notified if the event has already been dispatched.
**/

let loaded:Signal<string> = new Signal<string>(true);
loaded.listen((value:string) => console.log(value));
loaded.dispatch('Loading complete');
```

## SignalMonitor

Use SignalMonitor to wait for several signals to be dispatched.

```
let monitor:SignalMonitor = new SignalMonitor();
monitor.add(signal1);
monitor.add(signal2);
monitor.allSignalsDispatched.listen(() => console.log('All done!'));
monitor.start();
```
