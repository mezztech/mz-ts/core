/**
 * Animation easing function type.
 *
 * Resource: http://www.timotheegroleau.com/Flash/experiments/easing_function_generator.htm
 *
 * t - the elapsed time.
 * b - the starting value. This is the value you would get if t = 0.
 * c - the total 'change' in value from start to finish. So if you want to transition from 34 to 56 then c = (56-34) = 13.
 * d - the duration of the transition. If you want the transition to last, for example, 2 seconds then d = 2.
 */
export default interface IAnimationEaseFunction { (t:number, b:number, c:number, d:number):number; }
