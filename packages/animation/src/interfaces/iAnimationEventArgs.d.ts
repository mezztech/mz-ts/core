import IAnimation from './iAnimation';
import IAnimationValue from './iAnimationValue';
export interface IAnimationEventArgs {
    animation: IAnimation;
    values: IAnimationValue[];
    elapsedMilliseconds: number;
    progress: number;
}
export default IAnimationEventArgs;
