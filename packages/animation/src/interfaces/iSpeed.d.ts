import { SpeedType } from '../animationEnums';
import IAnimationEaseFunction from './iAnimationEaseFunction';
export interface ISpeed {
    isDefault: boolean;
    type: SpeedType;
    ease: IAnimationEaseFunction;
    duration: number;
    unitsPerSecond: number;
    differenceFactor: number;
    unitValue: number;
    normalizedUnitValue: number;
    getDuration(startValue?: number, targetValue?: number, isNormalizedValue?: boolean, fps?: number): number;
}
export default ISpeed;
