import IAnimation from './iAnimation';
import IAnimationValue from './iAnimationValue';


export interface IAnimationEventArgs {
    animation:IAnimation;
    values:IAnimationValue[];
    elapsedMilliseconds:number;


    /**
     * The normalized progress of the animation (0.0 <--> 1.0).
     */
    progress:number;
} // End interface


export default IAnimationEventArgs;
