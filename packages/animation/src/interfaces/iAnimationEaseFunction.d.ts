export default interface IAnimationEaseFunction {
    (t: number, b: number, c: number, d: number): number;
}
