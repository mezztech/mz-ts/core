"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SpeedType;
(function (SpeedType) {
    SpeedType[SpeedType["DURATION"] = 0] = "DURATION";
    SpeedType[SpeedType["UNITS_PER_SECOND"] = 1] = "UNITS_PER_SECOND";
    SpeedType[SpeedType["DIFFERENCE_FACTOR"] = 2] = "DIFFERENCE_FACTOR";
})(SpeedType = exports.SpeedType || (exports.SpeedType = {}));
var AnimationEaseCategory;
(function (AnimationEaseCategory) {
    AnimationEaseCategory[AnimationEaseCategory["LINEAR"] = 0] = "LINEAR";
    AnimationEaseCategory[AnimationEaseCategory["QUADRATIC"] = 1] = "QUADRATIC";
    AnimationEaseCategory[AnimationEaseCategory["CUBIC"] = 2] = "CUBIC";
    AnimationEaseCategory[AnimationEaseCategory["QUARTIC"] = 3] = "QUARTIC";
    AnimationEaseCategory[AnimationEaseCategory["QUINTIC"] = 4] = "QUINTIC";
    AnimationEaseCategory[AnimationEaseCategory["SINE"] = 5] = "SINE";
    AnimationEaseCategory[AnimationEaseCategory["EXPONENTIAL"] = 6] = "EXPONENTIAL";
    AnimationEaseCategory[AnimationEaseCategory["CIRCULAR"] = 7] = "CIRCULAR";
    AnimationEaseCategory[AnimationEaseCategory["ELASTIC"] = 8] = "ELASTIC";
    AnimationEaseCategory[AnimationEaseCategory["BACK"] = 9] = "BACK";
    AnimationEaseCategory[AnimationEaseCategory["BOUNCE"] = 10] = "BOUNCE";
})(AnimationEaseCategory = exports.AnimationEaseCategory || (exports.AnimationEaseCategory = {}));
var AnimationEaseType;
(function (AnimationEaseType) {
    AnimationEaseType[AnimationEaseType["NONE"] = 0] = "NONE";
    AnimationEaseType[AnimationEaseType["IN"] = 1] = "IN";
    AnimationEaseType[AnimationEaseType["OUT"] = 2] = "OUT";
    AnimationEaseType[AnimationEaseType["INOUT"] = 3] = "INOUT";
})(AnimationEaseType = exports.AnimationEaseType || (exports.AnimationEaseType = {}));
//# sourceMappingURL=animationEnums.js.map