"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const animationEnums_1 = require("./animationEnums");
const back_1 = require("./easing/back");
const bounce_1 = require("./easing/bounce");
const circular_1 = require("./easing/circular");
const cubic_1 = require("./easing/cubic");
const elastic_1 = require("./easing/elastic");
const exponential_1 = require("./easing/exponential");
const linear_1 = require("./easing/linear");
const quadratic_1 = require("./easing/quadratic");
const quartic_1 = require("./easing/quartic");
const quintic_1 = require("./easing/quintic");
const sine_1 = require("./easing/sine");
class AnimationEase {
    static get defaultEase() { return quintic_1.default.inOut; }
    static getEasingFunction(easeCategory, easeType) {
        switch (easeCategory) {
            case animationEnums_1.AnimationEaseCategory.LINEAR:
                switch (easeType) {
                    case animationEnums_1.AnimationEaseType.IN:
                        return linear_1.default.in;
                    case animationEnums_1.AnimationEaseType.OUT:
                        return linear_1.default.out;
                    case animationEnums_1.AnimationEaseType.INOUT:
                        return linear_1.default.inOut;
                    case animationEnums_1.AnimationEaseType.NONE:
                        return linear_1.default.none;
                }
                break;
            case animationEnums_1.AnimationEaseCategory.QUADRATIC:
                switch (easeType) {
                    case animationEnums_1.AnimationEaseType.IN:
                        return quadratic_1.default.in;
                    case animationEnums_1.AnimationEaseType.OUT:
                        return quadratic_1.default.out;
                    case animationEnums_1.AnimationEaseType.INOUT:
                        return quadratic_1.default.inOut;
                    case animationEnums_1.AnimationEaseType.NONE:
                        break;
                }
                break;
            case animationEnums_1.AnimationEaseCategory.CUBIC:
                switch (easeType) {
                    case animationEnums_1.AnimationEaseType.IN:
                        return cubic_1.default.in;
                    case animationEnums_1.AnimationEaseType.OUT:
                        return cubic_1.default.out;
                    case animationEnums_1.AnimationEaseType.INOUT:
                        return cubic_1.default.inOut;
                    case animationEnums_1.AnimationEaseType.NONE:
                        break;
                }
                break;
            case animationEnums_1.AnimationEaseCategory.QUARTIC:
                switch (easeType) {
                    case animationEnums_1.AnimationEaseType.IN:
                        return quartic_1.default.in;
                    case animationEnums_1.AnimationEaseType.OUT:
                        return quartic_1.default.out;
                    case animationEnums_1.AnimationEaseType.INOUT:
                        return quartic_1.default.inOut;
                    case animationEnums_1.AnimationEaseType.NONE:
                        break;
                }
                break;
            case animationEnums_1.AnimationEaseCategory.QUINTIC:
                switch (easeType) {
                    case animationEnums_1.AnimationEaseType.IN:
                        return quintic_1.default.in;
                    case animationEnums_1.AnimationEaseType.OUT:
                        return quintic_1.default.out;
                    case animationEnums_1.AnimationEaseType.INOUT:
                        return quintic_1.default.inOut;
                    case animationEnums_1.AnimationEaseType.NONE:
                        break;
                }
                break;
            case animationEnums_1.AnimationEaseCategory.SINE:
                switch (easeType) {
                    case animationEnums_1.AnimationEaseType.IN:
                        return sine_1.default.in;
                    case animationEnums_1.AnimationEaseType.OUT:
                        return sine_1.default.out;
                    case animationEnums_1.AnimationEaseType.INOUT:
                        return sine_1.default.inOut;
                    case animationEnums_1.AnimationEaseType.NONE:
                        break;
                }
                break;
            case animationEnums_1.AnimationEaseCategory.EXPONENTIAL:
                switch (easeType) {
                    case animationEnums_1.AnimationEaseType.IN:
                        return exponential_1.default.in;
                    case animationEnums_1.AnimationEaseType.OUT:
                        return exponential_1.default.out;
                    case animationEnums_1.AnimationEaseType.INOUT:
                        return exponential_1.default.inOut;
                    case animationEnums_1.AnimationEaseType.NONE:
                        break;
                }
                break;
            case animationEnums_1.AnimationEaseCategory.CIRCULAR:
                switch (easeType) {
                    case animationEnums_1.AnimationEaseType.IN:
                        return circular_1.default.in;
                    case animationEnums_1.AnimationEaseType.OUT:
                        return circular_1.default.out;
                    case animationEnums_1.AnimationEaseType.INOUT:
                        return circular_1.default.inOut;
                    case animationEnums_1.AnimationEaseType.NONE:
                        break;
                }
                break;
            case animationEnums_1.AnimationEaseCategory.ELASTIC:
                switch (easeType) {
                    case animationEnums_1.AnimationEaseType.IN:
                        return elastic_1.default.in;
                    case animationEnums_1.AnimationEaseType.OUT:
                        return elastic_1.default.out;
                    case animationEnums_1.AnimationEaseType.INOUT:
                        return elastic_1.default.inOut;
                    case animationEnums_1.AnimationEaseType.NONE:
                        break;
                }
                break;
            case animationEnums_1.AnimationEaseCategory.BACK:
                switch (easeType) {
                    case animationEnums_1.AnimationEaseType.IN:
                        return back_1.default.in;
                    case animationEnums_1.AnimationEaseType.OUT:
                        return back_1.default.out;
                    case animationEnums_1.AnimationEaseType.INOUT:
                        return back_1.default.inOut;
                    case animationEnums_1.AnimationEaseType.NONE:
                        break;
                }
                break;
            case animationEnums_1.AnimationEaseCategory.BOUNCE:
                switch (easeType) {
                    case animationEnums_1.AnimationEaseType.IN:
                        return bounce_1.default.in;
                    case animationEnums_1.AnimationEaseType.OUT:
                        return bounce_1.default.out;
                    case animationEnums_1.AnimationEaseType.INOUT:
                        return bounce_1.default.inOut;
                    case animationEnums_1.AnimationEaseType.NONE:
                        break;
                }
                break;
        }
        return linear_1.default.none;
    }
}
exports.AnimationEase = AnimationEase;
exports.default = AnimationEase;
//# sourceMappingURL=animationEase.js.map