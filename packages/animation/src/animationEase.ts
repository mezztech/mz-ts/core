import { AnimationEaseCategory, AnimationEaseType } from './animationEnums';
import AnimationEaseBack from './easing/back';
import AnimationEaseBounce from './easing/bounce';
import AnimationEaseCircular from './easing/circular';
import AnimationEaseCubic from './easing/cubic';
import AnimationEaseElastic from './easing/elastic';
import AnimationEaseExponential from './easing/exponential';
import AnimationEaseLinear from './easing/linear';
import AnimationEaseQuadratic from './easing/quadratic';
import AnimationEaseQuartic from './easing/quartic';
import AnimationEaseQuintic from './easing/quintic';
import AnimationEaseSine from './easing/sine';
import IAnimationEaseFunction from './interfaces/iAnimationEaseFunction';


/**
 * Animation easing function type.
 *
 * Resource: http://www.timotheegroleau.com/Flash/experiments/easing_function_generator.htm
 *
 * t - the elapsed time.
 * b - the starting value. This is the value you would get if t = 0.
 * c - the total 'change' in value from start to finish. So if you want to transition from 34 to 56 then c = (56-34) = 13.
 * d - the duration of the transition. If you want the transition to last, for example, 2 seconds then d = 2.
 */
export class AnimationEase {
    static get defaultEase():IAnimationEaseFunction { return AnimationEaseQuintic.inOut; }


    static getEasingFunction(easeCategory:AnimationEaseCategory, easeType:AnimationEaseType):IAnimationEaseFunction  {
        switch (easeCategory) {
            case AnimationEaseCategory.LINEAR:
                switch (easeType) {
                    case AnimationEaseType.IN:
                        return AnimationEaseLinear.in;
                    case AnimationEaseType.OUT:
                        return AnimationEaseLinear.out;
                    case AnimationEaseType.INOUT:
                        return AnimationEaseLinear.inOut;
                    case AnimationEaseType.NONE:
                        return AnimationEaseLinear.none;
                }
                break;
            case AnimationEaseCategory.QUADRATIC:
                switch (easeType) {
                    case AnimationEaseType.IN:
                        return AnimationEaseQuadratic.in;
                    case AnimationEaseType.OUT:
                        return AnimationEaseQuadratic.out;
                    case AnimationEaseType.INOUT:
                        return AnimationEaseQuadratic.inOut;
                    case AnimationEaseType.NONE:
                        break;
                }
                break;
            case AnimationEaseCategory.CUBIC:
                switch (easeType) {
                    case AnimationEaseType.IN:
                        return AnimationEaseCubic.in;
                    case AnimationEaseType.OUT:
                        return AnimationEaseCubic.out;
                    case AnimationEaseType.INOUT:
                        return AnimationEaseCubic.inOut;
                    case AnimationEaseType.NONE:
                        break;
                }
                break;
            case AnimationEaseCategory.QUARTIC:
                switch (easeType) {
                    case AnimationEaseType.IN:
                        return AnimationEaseQuartic.in;
                    case AnimationEaseType.OUT:
                        return AnimationEaseQuartic.out;
                    case AnimationEaseType.INOUT:
                        return AnimationEaseQuartic.inOut;
                    case AnimationEaseType.NONE:
                        break;
                }
                break;
            case AnimationEaseCategory.QUINTIC:
                switch (easeType) {
                    case AnimationEaseType.IN:
                        return AnimationEaseQuintic.in;
                    case AnimationEaseType.OUT:
                        return AnimationEaseQuintic.out;
                    case AnimationEaseType.INOUT:
                        return AnimationEaseQuintic.inOut;
                    case AnimationEaseType.NONE:
                        break;
                }
                break;
            case AnimationEaseCategory.SINE:
                switch (easeType) {
                    case AnimationEaseType.IN:
                        return AnimationEaseSine.in;
                    case AnimationEaseType.OUT:
                        return AnimationEaseSine.out;
                    case AnimationEaseType.INOUT:
                        return AnimationEaseSine.inOut;
                    case AnimationEaseType.NONE:
                        break;
                }
                break;
            case AnimationEaseCategory.EXPONENTIAL:
                switch (easeType) {
                    case AnimationEaseType.IN:
                        return AnimationEaseExponential.in;
                    case AnimationEaseType.OUT:
                        return AnimationEaseExponential.out;
                    case AnimationEaseType.INOUT:
                        return AnimationEaseExponential.inOut;
                    case AnimationEaseType.NONE:
                        break;
                }
                break;
            case AnimationEaseCategory.CIRCULAR:
                switch (easeType) {
                    case AnimationEaseType.IN:
                        return AnimationEaseCircular.in;
                    case AnimationEaseType.OUT:
                        return AnimationEaseCircular.out;
                    case AnimationEaseType.INOUT:
                        return AnimationEaseCircular.inOut;
                    case AnimationEaseType.NONE:
                        break;
                }
                break;
            case AnimationEaseCategory.ELASTIC:
                switch (easeType) {
                    case AnimationEaseType.IN:
                        return AnimationEaseElastic.in;
                    case AnimationEaseType.OUT:
                        return AnimationEaseElastic.out;
                    case AnimationEaseType.INOUT:
                        return AnimationEaseElastic.inOut;
                    case AnimationEaseType.NONE:
                        break;
                }
                break;
            case AnimationEaseCategory.BACK:
                switch (easeType) {
                    case AnimationEaseType.IN:
                        return AnimationEaseBack.in;
                    case AnimationEaseType.OUT:
                        return AnimationEaseBack.out;
                    case AnimationEaseType.INOUT:
                        return AnimationEaseBack.inOut;
                    case AnimationEaseType.NONE:
                        break;
                }
                break;
            case AnimationEaseCategory.BOUNCE:
                switch (easeType) {
                    case AnimationEaseType.IN:
                        return AnimationEaseBounce.in;
                    case AnimationEaseType.OUT:
                        return AnimationEaseBounce.out;
                    case AnimationEaseType.INOUT:
                        return AnimationEaseBounce.inOut;
                    case AnimationEaseType.NONE:
                        break;
                }
                break;
        }

        return AnimationEaseLinear.none;
    }
} // End class


export default AnimationEase;
