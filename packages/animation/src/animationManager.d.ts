import { List } from '@mezzy/collections';
import { Signal } from '@mezzy/signals';
import { HiResTimer } from '@mezzy/timer';
import IAnimation from './interfaces/iAnimation';
export declare class AnimationManager {
    constructor(timer: HiResTimer);
    animations: List<IAnimation>;
    protected p_completedAnimations: List<IAnimation>;
    readonly isAllAnimationCompleted: boolean;
    private _isAllAnimationCompleted;
    length(): number;
    private _lengthCurrent;
    timer: HiResTimer;
    protected p_timer: HiResTimer;
    ticked: Signal<number>;
    allAnimationsCompleted: Signal<any>;
    tick(): void;
    stop(): void;
    start(): void;
    readonly isRunning: boolean;
    protected p_isRunning: boolean;
    run(animation: IAnimation): IAnimation;
    delete(animation: IAnimation): void;
    clear(): void;
    interruptAll(): void;
}
export default AnimationManager;
