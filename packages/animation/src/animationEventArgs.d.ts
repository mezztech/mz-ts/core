import IAnimationValue from './interfaces/iAnimationValue';
import IAnimation from './interfaces/iAnimation';
export declare class AnimationEventArgs {
    constructor(animation: IAnimation, progress: number, elapsedMilliseconds?: number, values?: IAnimationValue[]);
    animation: IAnimation;
    progress: number;
    elapsedMilliseconds: number;
    values: IAnimationValue[];
}
export default AnimationEventArgs;
