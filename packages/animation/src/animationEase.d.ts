import { AnimationEaseCategory, AnimationEaseType } from './animationEnums';
import IAnimationEaseFunction from './interfaces/iAnimationEaseFunction';
export declare class AnimationEase {
    static readonly defaultEase: IAnimationEaseFunction;
    static getEasingFunction(easeCategory: AnimationEaseCategory, easeType: AnimationEaseType): IAnimationEaseFunction;
}
export default AnimationEase;
