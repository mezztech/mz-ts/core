import ISpeed from './interfaces/iSpeed';
import { SpeedType } from './animationEnums';
import IAnimationEaseFunction from './interfaces/iAnimationEaseFunction';
export declare class Speed implements ISpeed {
    constructor(data?: (Speed | Object | string));
    static validate(durationSpeed?: (number | ISpeed)): ISpeed;
    readonly isDefault: boolean;
    private _isDefault;
    type: SpeedType;
    private _speedType;
    ease: IAnimationEaseFunction;
    duration: number;
    private _duration;
    unitsPerSecond: number;
    private _unitsPerSecond;
    differenceFactor: number;
    private _differenceFactor;
    unitValue: number;
    private _unitValue;
    normalizedUnitValue: number;
    private _normalizedUnitValue;
    getDuration(startValue?: number, targetValue?: number, isNormalizedValue?: boolean, fps?: number): number;
}
export default Speed;
