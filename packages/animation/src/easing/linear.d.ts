export declare class AnimationEaseLinear {
    static none(t: number, b: number, c: number, d: number): number;
    static in(t: number, b: number, c: number, d: number): number;
    static out(t: number, b: number, c: number, d: number): number;
    static inOut(t: number, b: number, c: number, d: number): number;
}
export default AnimationEaseLinear;
