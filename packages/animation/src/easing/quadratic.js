"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AnimationEaseQuadratic {
    static in(t, b, c, d) {
        return c * (t /= d) * t + b;
    }
    static out(t, b, c, d) {
        return -c * (t /= d) * (t - 2) + b;
    }
    static inOut(t, b, c, d) {
        if ((t /= d * 0.5) < 1.0)
            return c * 0.5 * t * t + b;
        return -c * 0.5 * ((--t) * (t - 2) - 1) + b;
    }
}
exports.AnimationEaseQuadratic = AnimationEaseQuadratic;
exports.default = AnimationEaseQuadratic;
//# sourceMappingURL=quadratic.js.map