export class AnimationEaseBack {
    static in(t:number, b:number, c:number, d:number):number {
        return AnimationEaseBack.inExtended(t, b, c, d, 1.70158);
    }


    static inExtended(t:number, b:number, c:number, d:number, s:number):number {
        return c * (t /= d) * t * ((s + 1) * t - s) + b;
    }


    static out(t:number, b:number, c:number, d:number):number {
        return AnimationEaseBack.outExtended(t, b, c, d, 1.70158);
    }


    static outExtended(t:number, b:number, c:number, d:number, s:number):number {
        return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
    }


    static inOut(t:number, b:number, c:number, d:number):number {
        return AnimationEaseBack.inOutExtended(t, b, c, d, 1.70158);
    }


    static inOutExtended(t:number, b:number, c:number, d:number, s:number):number {
        if ((t /= d * 0.5) < 1) return c * 0.5 * (t * t * (((s *= (1.525)) + 1) * t - s)) + b;
        return c * 0.5 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2) + b;
    }
} // End class


export default AnimationEaseBack;
