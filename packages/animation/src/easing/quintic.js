"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AnimationEaseQuintic {
    static in(t, b, c, d) {
        return c * (t /= d) * t * t * t * t + b;
    }
    static out(t, b, c, d) {
        return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
    }
    static inOut(t, b, c, d) {
        if ((t /= d * 0.5) < 1.0)
            return c * 0.5 * t * t * t * t * t + b;
        return c * 0.5 * ((t -= 2) * t * t * t * t + 2) + b;
    }
}
exports.AnimationEaseQuintic = AnimationEaseQuintic;
exports.default = AnimationEaseQuintic;
//# sourceMappingURL=quintic.js.map