"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AnimationEaseBack {
    static in(t, b, c, d) {
        return AnimationEaseBack.inExtended(t, b, c, d, 1.70158);
    }
    static inExtended(t, b, c, d, s) {
        return c * (t /= d) * t * ((s + 1) * t - s) + b;
    }
    static out(t, b, c, d) {
        return AnimationEaseBack.outExtended(t, b, c, d, 1.70158);
    }
    static outExtended(t, b, c, d, s) {
        return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
    }
    static inOut(t, b, c, d) {
        return AnimationEaseBack.inOutExtended(t, b, c, d, 1.70158);
    }
    static inOutExtended(t, b, c, d, s) {
        if ((t /= d * 0.5) < 1)
            return c * 0.5 * (t * t * (((s *= (1.525)) + 1) * t - s)) + b;
        return c * 0.5 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2) + b;
    }
}
exports.AnimationEaseBack = AnimationEaseBack;
exports.default = AnimationEaseBack;
//# sourceMappingURL=back.js.map