export class AnimationEaseLinear {
    static none(t:number, b:number, c:number, d:number):number {
        t /= d;
        return b + c * (t);
    }


    static in(t:number, b:number, c:number, d:number):number {
        return c * t / d + b;
    }


    static out(t:number, b:number, c:number, d:number):number {
        return c * t / d + b;
    }


    static inOut(t:number, b:number, c:number, d:number):number {
        return c * t / d + b;
    }
} // End class


export default AnimationEaseLinear;
