import { IPromise, Result } from '@mezzy/result';
import { Signal } from '@mezzy/signals';
import IInitializable from './iInitializable';
export declare class Initializable implements IInitializable {
    constructor();
    readonly isInitialized: boolean;
    protected p_isInitialized: boolean;
    initialized: Signal<any>;
    protected p_initializedValue: any;
    initialize(...args: any[]): any;
    protected p_initialize(args: any[]): IPromise<any>;
    resetInitialization(): void;
    protected p_onInitializeStarted(result: Result<any>, args: any[]): IPromise<any>;
    protected p_onInitialized(value: any): void;
}
export default Initializable;
