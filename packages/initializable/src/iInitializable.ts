import { IPromise, Result } from '@mezzy/result';
import { Signal } from '@mezzy/signals';


export interface IInitializable {
    isInitialized:boolean;
    initialized:Signal<any>;
    initialize(...args:any[]):IPromise<any>;
    resetInitialization():void;
} // End interface


export default IInitializable;
