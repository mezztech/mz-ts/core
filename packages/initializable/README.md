# initializable

Base class for classes that support initialize methods.

## Install

sudo npm install --save @mezzy/initializable

## Usage

```
import {IInitializable, Initializable} from '@mezzy/initializable';
```
