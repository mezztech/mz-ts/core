import IInitializable from './src/iInitializable';
import Initializable from './src/initializable';
export { IInitializable, Initializable };
export default Initializable;
