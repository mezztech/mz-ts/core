import { Expect, Test, TestCase, TestFixture } from 'alsatian';


import {
    Initializable
} from '../index';


class InitMe extends Initializable {}


@TestFixture("Initializable Tests")
export class Tests {
    @Test("instantiation")
    test01() {
        let initializable:InitMe = new InitMe();
        initializable.initialize();
        Expect(initializable.isInitialized).toBe(true);
    }
}
