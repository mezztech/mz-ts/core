import ProgressCommand from './progressCommand';
export declare class ProgressReport {
    constructor(commandCurrent: ProgressCommand, stepsTotal: number, stepCurrent: number, weightTotal: number, weightCompleted: number, weightCurrent: number, isStepCompleted: boolean, timeToCompleteStep: number, timeElapsedSinceStart: number, progress: number, progressUponStepCompletion: number, percent: number, message: string);
    commandCurrent: ProgressCommand;
    stepsTotal: number;
    stepCurrent: number;
    isStepCompleted: boolean;
    timeToCompleteStep: number;
    timeElapsedSinceStart: number;
    weightTotal: number;
    weightCompleted: number;
    weightCurrent: number;
    progress: number;
    progressUponStepCompletion: number;
    percent: number;
    message: string;
    log(): void;
}
export default ProgressReport;
