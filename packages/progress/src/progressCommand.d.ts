import { IPromise } from '@mezzy/result';
import { Command } from '@mezzy/commands';
import ProgressTracker from './progressTracker';
import IProgressRunFunction from './iProgressRunFunction';
export declare class ProgressCommand extends Command {
    constructor(progressTracker: ProgressTracker, runFunction?: IProgressRunFunction, scope?: any, message?: string, weight?: number);
    parentTracker: ProgressTracker;
    weight: number;
    subTracker: ProgressTracker;
    run(...args: any[]): IPromise<any>;
}
export default ProgressCommand;
