import ProgressRunArgs from './progressRunArgs';
export interface IProgressRunFunction { (args:ProgressRunArgs, lastProgressCommandReturnValue:any, ...runArgs:any[]):any; }
export default IProgressRunFunction;
