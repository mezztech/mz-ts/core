# http

Ajax tools.

## Install

sudo npm install --save @mezzy/http

## Usage

```
import {Http} from '@mezzy/http';

Http.getBlob(blobUrl).then((blob:Blob) => {
    /// Do something with the blob.
});
```
