/**
 * IMPORTANT:
 * is needs to be completely free of any dependencies,
 * so don't use other LUX packages for testing here.
 */


import {Expect, Test, TestCase, TestFixture} from 'alsatian';
import is from '../index';


@TestFixture('is Tests')
export class Tests {
    @TestCase(null)
    @TestCase(undefined)
    @TestCase('')
    @TestCase(NaN)
    @TestCase()
    @Test('Empty')
    test01(testValue:any) {
        Expect(is.empty(testValue)).toBe(true);
    }


    @TestCase(0)
    @TestCase(1)
    @TestCase(-1)
    @TestCase('hello')
    @TestCase(false)
    @Test('Not Empty')
    test02(testValue:any) { Expect(is.notEmpty(testValue)).toBe(true); }


    @TestCase(/"(?:\\["\\]|[^\n"\\])*"/)
    @TestCase(/[ \t]+/)
    @TestCase(/[-\/\\^$*+?.()|[\]{}]/g)
    @Test('RegExp')
    test03(testValue:any) {
        Expect(is.regExp(testValue)).toBe(true);
    }


    @TestCase(0)
    @TestCase(1)
    @TestCase(-1)
    @TestCase('hello')
    @TestCase(false)
    @Test('Not RegExp')
    test04(testValue:any) {
        Expect(is.regExp(testValue)).toBe(false);
    }


    @TestCase([0, 1, -1, 'hello', false])
    @Test('Array')
    test05(testValue:any) {
        Expect(is.array(testValue)).toBe(true);
    }


    @TestCase(0)
    @TestCase(1)
    @TestCase(-1)
    @TestCase('hello')
    @TestCase(false)
    @Test('Not array')
    test06(testValue:any) {
        Expect(is.array(testValue)).toBe(false);
    }


    @TestCase({property01: 'boo', property02: 3})
    @TestCase({"property01": "boo", "property02": 3})
    @TestCase({})
    @Test('Object')
    test07(testValue:any) {
        Expect(is.object(testValue)).toBe(true);
    }


    @TestCase(0)
    @TestCase(1)
    @TestCase(-1)
    @TestCase('hello')
    @TestCase(false)
    @Test('Not object')
    test08(testValue:any) {
        Expect(is.object(testValue)).toBe(false);
    }


    @TestCase({property01: 'boo', property02: 3})
    @Test('Non-empty object')
    test09(testValue:any) {
        Expect(is.nonEmptyObject(testValue)).toBe(true);
    }


    @TestCase({})
    @Test('Empty object')
    test10(testValue:any) {
        Expect(is.emptyObject(testValue)).toBe(true);
    }


    @TestCase(0)
    @TestCase(1)
    @TestCase(-1)
    @Test('Is number true')
    testIsumberTrue(testValue:any) {
        Expect(is.number(testValue)).toBe(true);
    }


    @TestCase('hello')
    @TestCase(false)
    @Test('Is number false')
    testIsumberFalse(testValue:any) {
        Expect(is.number(testValue)).toBe(false);
    }
}
