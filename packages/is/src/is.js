"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class is {
    static empty(value, isTestEmptyString = true) {
        try {
            return (this.undefined(value) ||
                typeof value === 'undefined' ||
                value === null ||
                value === undefined ||
                (isTestEmptyString && value === '') ||
                this.NaN(value));
        }
        catch (e) {
            return true;
        }
    }
    static notEmpty(value, isTestEmptyString = true) { return !this.empty(value, isTestEmptyString); }
    static emptyAny(...values) {
        let isAnyValueEmpty = false;
        values.forEach((value) => {
            if (this.empty(value)) {
                isAnyValueEmpty = true;
                return false;
            }
            return true;
        });
        return isAnyValueEmpty;
    }
    static notEmptyAny(...values) { return !this.emptyAny(...values); }
    static emptyDeep(rootObject, deepChild) {
        if (this.empty(rootObject) || this.empty(deepChild))
            return true;
        let obj = rootObject;
        let isEmpty = false;
        deepChild.split('.').every((child) => {
            if (this.empty(obj)) {
                isEmpty = true;
                return false;
            }
            obj = obj[child];
            return true;
        });
        return isEmpty;
    }
    static notEmptyDeep(rootObject, deepChild) {
        return !this.emptyDeep(rootObject, deepChild);
    }
    static emptyDeepAny(rootObject, ...deepChildren) {
        let isAnyDeepChildEmpty = false;
        deepChildren.forEach((deepChild) => {
            if (this.emptyDeep(rootObject, deepChild)) {
                isAnyDeepChildEmpty = true;
                return false;
            }
            return true;
        });
        return isAnyDeepChildEmpty;
    }
    static notEmptyDeepAny(rootObject, ...deepChildren) {
        return !this.emptyDeepAny(rootObject, ...deepChildren);
    }
    static array(value) { return Array.isArray(value); }
    static regExp(value) { return this.notEmpty(value, true) && value !== 0 && value instanceof RegExp; }
    static object(value) {
        let isObject = this.notEmpty(value) &&
            typeof value === 'object' &&
            !this.regExp(value) &&
            !this.array(value);
        return isObject;
    }
    static emptyObject(value) { return this.object(value) && Object.keys(value).length === 0; }
    static nonEmptyObject(value) { return this.object(value) && !this.emptyObject(value); }
    static number(value) { return !isNaN(parseFloat(value)) && isFinite(value); }
    static notNumber(value) { return !this.number(value); }
    static NaN(value) {
        return value !== value;
    }
    static undefined(value) { return value === void 0; }
}
exports.is = is;
exports.default = is;
//# sourceMappingURL=is.js.map