export declare class is {
    static empty(value: any, isTestEmptyString?: boolean): boolean;
    static notEmpty(value: any, isTestEmptyString?: boolean): boolean;
    static emptyAny(...values: any[]): boolean;
    static notEmptyAny(...values: any[]): boolean;
    static emptyDeep(rootObject: any, deepChild: string): boolean;
    static notEmptyDeep(rootObject: any, deepChild: string): boolean;
    static emptyDeepAny(rootObject: any, ...deepChildren: string[]): boolean;
    static notEmptyDeepAny(rootObject: any, ...deepChildren: string[]): boolean;
    static array(value: any): boolean;
    static regExp(value: any): boolean;
    static object(value: any): boolean;
    static emptyObject(value: any): boolean;
    static nonEmptyObject(value: any): boolean;
    static number(value: any): boolean;
    static notNumber(value: any): boolean;
    static NaN(value: any): boolean;
    static undefined(value: any): boolean;
}
export default is;
