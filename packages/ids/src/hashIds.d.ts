export declare class HashIds {
    constructor(salt?: string, minLength?: number, alphabet?: string);
    private _salt;
    private _minLength;
    private _alphabet;
    private _guards;
    private _seps;
    encode(...args: (number | number[])[]): string;
    decode(encodedString: string): number[];
    encodeHex(hex: any): string;
    decodeHex(encodedHex: string): string;
    private _encode;
    private _decode;
    private _shuffle;
    private _toAlphabet;
    private _fromAlphabet;
    private _escapeRegExp;
    private _parseInt;
}
export default HashIds;
