# ids

Generates unique IDs

## Install

`sudo npm install --save @mezzy/ids`

## Usage

```
import {Identifier} from '@mezzy/ids';

let id:string = Identifier.generate();
```
