import Result from './result';
export declare class ResultHandler<TValue> {
    constructor(onDone: (value: TValue) => any);
    onDone: (value: TValue) => any;
    result: Result<any>;
    handle(value: TValue): void;
}
export default ResultHandler;
