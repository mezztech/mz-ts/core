# flags

An enum-based flag system, with methods for dynamically adding and removing named flags.

## Install

sudo npm install --save @mezzy/flags

## Usage

```
import {EnumFlags, FlagEnumGenerator} from '@mezzy/flags';
```
