import IEnum from './interfaces/iEnum';
import IEnumFlags from './interfaces/iEnumFlags';
import IFlagEnumGenerator from './interfaces/iFlagEnumGenerator';
export declare class EnumFlags<TEnum> implements IEnumFlags {
    constructor(baseEnum: IEnum, value: (number | string));
    baseEnum: IFlagEnumGenerator;
    value: number;
    private _value;
    toString(): string;
    toArray(): IEnumFlags[];
    has(value: (number | string)): boolean;
    set(value: (number | string)): IEnumFlags;
    add(value: (number | string)): IEnumFlags;
    delete(value: (number | string)): IEnumFlags;
    intersect(value: (number | string)): IEnumFlags;
    equals(value: (number | string)): boolean;
}
export default EnumFlags;
