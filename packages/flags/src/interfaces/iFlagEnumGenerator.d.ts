import IEnum from './iEnum';
import IEnumFlags from './iEnumFlags';
export interface IFlagEnumGenerator extends IEnum {
    readonly flagCount: number;
    readonly maxValue: number;
    addFlag(flagName: string): IFlagEnumGenerator;
    flags(value?: (number | string)): IEnumFlags;
}
export default IFlagEnumGenerator;
