import IEnum from './iEnum';
import IEnumFlags from './iEnumFlags';


export interface IFlagEnumGenerator extends IEnum {
    readonly flagCount:number;


    /**
     * This will be set automatically to the highest numeric flag value,
     * so it can be used in flag calculations.
     */
    readonly maxValue:number;


    addFlag(flagName:string):IFlagEnumGenerator;


    /**
     * Returns a new EnumFlags instance, with the given value, that uses the base enum for flag calculations.
     */
    flags(value?:(number | string)):IEnumFlags;
}


export default IFlagEnumGenerator;
