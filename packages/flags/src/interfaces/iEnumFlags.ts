import IFlagEnumGenerator from './iFlagEnumGenerator';


export default interface IEnumFlags {
    baseEnum:IFlagEnumGenerator;


    /**
     * The current value, representing the combined flags.
     */
    value:number;


    /**
     * Returns an array of the individual enum flags that are represented by the value of this instance.
     */
    toArray():IEnumFlags[];

    /**
     * Does this instance contain all the flags represented in the provided value?
     */
    has(value:(number | string)):boolean;


    /**
     * Sets the flags represented by the current value of this instance to the flags represented by the provided value and returns a new instance.
     */
    set(value:(number | string)):IEnumFlags;


    /**
     * Adds the flags represented by the provided value to the flags represented by the current value of this instance and returns a new instance.
     */
    add(value:(number | string)):IEnumFlags;


    /**
     * Removes the flags represented by the provided value from the current value of this instance and returns a new instance.
     */
    delete(value:(number | string)):IEnumFlags;


    /**
     * Returns an instance containing all intersecting flags.
     */
    intersect(value:(number | string)):IEnumFlags;


    /**
     * Does the provided value represent the same flags held in the value of this instance?
     * @param value
     */
    equals(value:(number | string)):boolean;
} // End interface
