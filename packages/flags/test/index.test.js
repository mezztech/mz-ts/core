"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var alsatian_1 = require("alsatian");
var index_1 = require("./index");
var Tests = (function () {
    function Tests() {
    }
    Tests.prototype.setup = function () { };
    Tests.prototype.setupFixture = function () { };
    Tests.prototype.teardown = function () { };
    Tests.prototype.teardownFixture = function () { };
    Tests.prototype.test01 = function (testValue) {
        var TestFlags;
        (function (TestFlags) {
            TestFlags[TestFlags["SIGNAL_CHANGE"] = 0] = "SIGNAL_CHANGE";
            TestFlags[TestFlags["SAVE"] = 1] = "SAVE";
        })(TestFlags || (TestFlags = {}));
        var options = new index_1.FlagEnumGenerator(TestFlags);
        var flags = options.flags();
        alsatian_1.Expect(options.NONE).toBe(1);
        alsatian_1.Expect(options.SIGNAL_CHANGE).toBe(2);
        alsatian_1.Expect(options.SAVE).toBe(4);
        alsatian_1.Expect(flags.has(options.NONE)).toBe(true);
        alsatian_1.Expect(flags.has(options.SIGNAL_CHANGE)).toBe(false);
        alsatian_1.Expect(flags.has(options.SAVE)).toBe(false);
        alsatian_1.Expect(flags.value).toBe(1);
        flags.add(options.SIGNAL_CHANGE);
        alsatian_1.Expect(flags.has(options.NONE)).toBe(false);
        alsatian_1.Expect(flags.has(options.SIGNAL_CHANGE)).toBe(true);
        alsatian_1.Expect(flags.has(options.SAVE)).toBe(false);
        alsatian_1.Expect(flags.value).toBe(2);
        flags.add(options.SAVE);
        alsatian_1.Expect(flags.has(options.NONE)).toBe(false);
        alsatian_1.Expect(flags.has(options.SIGNAL_CHANGE)).toBe(true);
        alsatian_1.Expect(flags.has(options.SAVE)).toBe(true);
        alsatian_1.Expect(flags.value).toBe(6);
        flags.delete(options.SIGNAL_CHANGE);
        alsatian_1.Expect(flags.has(options.NONE)).toBe(false);
        alsatian_1.Expect(flags.has(options.SIGNAL_CHANGE)).toBe(false);
        alsatian_1.Expect(flags.has(options.SAVE)).toBe(true);
        alsatian_1.Expect(flags.value).toBe(4);
        flags.delete(options.SAVE);
        alsatian_1.Expect(flags.has(options.NONE)).toBe(true);
        alsatian_1.Expect(flags.has(options.SIGNAL_CHANGE)).toBe(false);
        alsatian_1.Expect(flags.has(options.SAVE)).toBe(false);
        alsatian_1.Expect(flags.value).toBe(1);
    };
    __decorate([
        alsatian_1.Setup,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], Tests.prototype, "setup", null);
    __decorate([
        alsatian_1.SetupFixture,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], Tests.prototype, "setupFixture", null);
    __decorate([
        alsatian_1.Teardown,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], Tests.prototype, "teardown", null);
    __decorate([
        alsatian_1.TeardownFixture,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], Tests.prototype, "teardownFixture", null);
    __decorate([
        alsatian_1.Test('Return correct flag'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Number]),
        __metadata("design:returntype", void 0)
    ], Tests.prototype, "test01", null);
    Tests = __decorate([
        alsatian_1.TestFixture('Flag Tests')
    ], Tests);
    return Tests;
}());
exports.Tests = Tests;
//# sourceMappingURL=index.test.js.map