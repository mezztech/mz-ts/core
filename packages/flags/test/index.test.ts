import {
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Expect
} from 'alsatian';


import {
    IEnum,
    IEnumFlags,
    EnumFlags,
    FlagEnumGenerator
} from '../index';

@TestFixture('Flag Tests')
export class Tests {
    @Setup
    public setup() {}

    @SetupFixture
    public setupFixture() {}

    @Teardown
    public teardown() {}

    @TeardownFixture
    public teardownFixture() {}

    @Test('Return correct flag')
    test01(testValue: number) {
        enum TestFlags {
            SIGNAL_CHANGE,
            SAVE
        } // End enum

        const options = new FlagEnumGenerator<TestFlags>(TestFlags);
        const flags = options.flags();

        Expect(options.NONE).toBe(1);
        Expect(options.SIGNAL_CHANGE).toBe(2);
        Expect(options.SAVE).toBe(4);

        Expect(flags.has(options.NONE)).toBe(true);
        Expect(flags.has(options.SIGNAL_CHANGE)).toBe(false);
        Expect(flags.has(options.SAVE)).toBe(false);
        Expect(flags.value).toBe(1);

        flags.add(options.SIGNAL_CHANGE);

        Expect(flags.has(options.NONE)).toBe(false);
        Expect(flags.has(options.SIGNAL_CHANGE)).toBe(true);
        Expect(flags.has(options.SAVE)).toBe(false);
        Expect(flags.value).toBe(2);

        flags.add(options.SAVE);

        Expect(flags.has(options.NONE)).toBe(false);
        Expect(flags.has(options.SIGNAL_CHANGE)).toBe(true);
        Expect(flags.has(options.SAVE)).toBe(true);
        Expect(flags.value).toBe(6);

        flags.delete(options.SIGNAL_CHANGE);

        Expect(flags.has(options.NONE)).toBe(false);
        Expect(flags.has(options.SIGNAL_CHANGE)).toBe(false);
        Expect(flags.has(options.SAVE)).toBe(true);
        Expect(flags.value).toBe(4);

        flags.delete(options.SAVE);

        Expect(flags.has(options.NONE)).toBe(true);
        Expect(flags.has(options.SIGNAL_CHANGE)).toBe(false);
        Expect(flags.has(options.SAVE)).toBe(false);
        Expect(flags.value).toBe(1);
    }
}
