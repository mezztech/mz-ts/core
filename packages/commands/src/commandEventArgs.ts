import Command from './command';


export class CommandEventArgs {
    constructor(command:Command, progress:number, returnValue?:any, ...args:any[]) {
        this.command = command;
        this.progress = progress;
        this.returnValue = returnValue;
        this.args = args;
    }


    command:Command;
    returnValue:any;
    totalRunning:number = 1;
    pending:number = 1;
    args:any[];
    isCommandComplete:boolean = false;


    /**
     * A normalized value representing progress while running queued commands.
     * The value starts at 0 when run() is initially called on the command manager,
     * and progresses to 1, once all the queued commands have been run and the command queue is empty.
     */
    progress:number;
} // End class


export default CommandEventArgs;
