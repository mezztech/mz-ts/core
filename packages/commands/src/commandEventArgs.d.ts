import Command from './command';
export declare class CommandEventArgs {
    constructor(command: Command, progress: number, returnValue?: any, ...args: any[]);
    command: Command;
    returnValue: any;
    totalRunning: number;
    pending: number;
    args: any[];
    isCommandComplete: boolean;
    progress: number;
}
export default CommandEventArgs;
