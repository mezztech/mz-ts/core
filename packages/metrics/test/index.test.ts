import is from '@mezzy/is';
import Debug from '@mezzy/debug';


import {
    AsyncTest,
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Timeout,
    Expect
} from 'alsatian';


import {
    Color
} from '../index';


@TestFixture("Metrics Tests")
export class Tests {
    @AsyncTest("To object")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    public async toObjectTest(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            let color:Color = new Color(255, 60, 10);
            Debug.data(color.toObject());

            resolve();
        });

        const result:any = await promise;
    }
}
