import IPoint from "./iPoint";
import ISize from "./iSize";
export default interface IRectangle extends IPoint, ISize {}
