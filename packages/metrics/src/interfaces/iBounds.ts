import IMetrics from "./iMetrics";
import IPoint3D from "./iPoint3D";
import IRectangle from "./IRectangle";
import ISize3D from "./ISize3D";


export default interface IBounds extends IMetrics, IPoint3D, IRectangle, ISize3D {
    reset():this;
    setPrevious(values?:number[]):this;


    readonly previous:number[];


    readonly position:number[];
    readonly size:number[];
    readonly pivot:number[];
    readonly scale:number[];


    pivotX:number;
    pivotY:number;
    pivotZ:number;
    scaleX:number;
    scaleY:number;
    scaleZ:number;


    x:number;
    leftUnscaled:number;
    left:number;
    rightUnscaled:number;
    right:number;


    y:number;
    topUnscaled:number
    top:number;
    bottomUnscaled:number;
    bottom:number;


    z:number;
    frontUnscaled:number
    front:number;
    readonly backUnscaled:number;
    readonly back:number;


    readonly topLeftFrontUnscaled:number[];
    readonly topRightFrontUnscaled:number[];
    readonly bottomLeftFrontUnscaled:number[];
    readonly bottomRightFrontUnscaled:number[];
    readonly topLeftFront:number[];
    readonly topRightFront:number[];
    readonly bottomLeftFront:number[];
    readonly bottomRightFront:number[];


    readonly topLeftBackUnscaled:number[];
    readonly topRightBackUnscaled:number[];
    readonly bottomLeftBackUnscaled:number[];
    readonly bottomRightBackUnscaled:number[];
    readonly topLeftBack:number[];
    readonly topRightBack:number[];
    readonly bottomLeftBack:number[];
    readonly bottomRightBack:number[];


    readonly centerXUnscaled:number;
    readonly centerYUnscaled:number;
    readonly centerZUnscaled:number;
    readonly centerX:number;
    readonly centerY:number;
    readonly centerZ:number;


    widthUnscaled:number;
    width:number;


    heightUnscaled:number;
    height:number;


    depthUnscaled:number;
    depth:number;


    scaleAll:number;


    radiusUnscaled:number;
    radius:number;
}
