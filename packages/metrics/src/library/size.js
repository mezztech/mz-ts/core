"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@mezzy/is");
const metricsBase_1 = require("../metricsBase");
const metrics_1 = require("../metrics");
class Size extends metricsBase_1.default {
    constructor(...values) { super(...values); }
    _setSchema() { this._schema = Size.schema; }
    get width() { return this._array[0]; }
    set width(value) { this._setValues([0], [value]); }
    get height() { return this._array[1]; }
    set height(value) { this._setValues([1], [value]); }
    get isEmpty() { return is_1.default.empty(this.width) || is_1.default.empty(this.width); }
    get copy() { return new Size(this.width, this.height); }
    static get empty() { return new Size(); }
    static get schema() { return ["width", "height"]; }
    static getArray(...args) { return metrics_1.default.getArray(args, Size.schema); }
}
exports.default = Size;
//# sourceMappingURL=size.js.map