import MetricsBase from "../metricsBase";
import IPoint from "../interfaces/iPoint";
export default class Point extends MetricsBase implements IPoint {
    constructor(...values: any[]);
    protected _setSchema(): void;
    x: number;
    y: number;
    readonly isEmpty: boolean;
    readonly copy: Point;
    static readonly empty: Point;
    static readonly schema: string[];
    static getArray(...args: number[]): number[];
}
