import is from "@mezzy/is";
import IRectangle from "../interfaces/iRectangle";
import Point from "./point";
import Metrics from "../metrics";


export default class Rectangle extends Point implements IRectangle {
    /// This is needed because base class constructor is protected.
    constructor(...values:any[]) { super(...values); }


    protected _setSchema():void { this._schema = Rectangle.schema; }


    get width():number { return this._array[2]; }
    set width(value:number) { this._setValues([2], [value]); }


    get height():number { return this._array[3]; }
    set height(value:number) { this._setValues([3], [value]); }


    get isEmpty():boolean {  return is.empty(this.x) || is.empty(this.y) || is.empty(this.width) || is.empty(this.height); }
    get copy():Rectangle { return new Rectangle(this.x, this.y, this.width, this.height); }


    /*====================================================================*
     START: Static Methods
     *====================================================================*/
    static get empty():Rectangle { return new Rectangle(); }
    static get schema():string[] { return ["x", "y", "width", "height"]; }
    static getArray(...args:number[]):number[] { return Metrics.getArray(args, Rectangle.schema); }
}
