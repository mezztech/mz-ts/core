import IRectangle from "../interfaces/iRectangle";
import Point from "./point";
export default class Rectangle extends Point implements IRectangle {
    constructor(...values: any[]);
    protected _setSchema(): void;
    width: number;
    height: number;
    readonly isEmpty: boolean;
    readonly copy: Rectangle;
    static readonly empty: Rectangle;
    static readonly schema: string[];
    static getArray(...args: number[]): number[];
}
