import is from "@mezzy/is";
import ICircle from "../interfaces/iCircle";
import Point from "./point";
import Metrics from "../metrics";


export default class Circle extends Point implements ICircle {
    /// This is needed because base class constructor is protected.
    constructor(...values:any[]) { super(...values); }


    protected _setSchema():void { this._schema = Circle.schema; }


    get radius():number { return this._array[2]; }
    set radius(value:number) { this._setValues([2], [value]); }


    get isEmpty():boolean {  return is.empty(this.x) || is.empty(this.y) || is.empty(this.radius); }
    get copy():Circle { return new Circle(this.x, this.y, this.radius); }


    /*====================================================================*
     START: Static Methods
     *====================================================================*/
    static get empty():Circle { return new Circle(); }
    static get schema():string[] { return ["x", "y", "radius"]; }
    static getArray(...args:number[]):number[] { return Metrics.getArray(args, Circle.schema); }
}
