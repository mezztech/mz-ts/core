import IVector from "../interfaces/iVector";
import Point3D from "./point3D";
export default class Vector extends Point3D implements IVector {
    constructor(...values: any[]);
    protected _setSchema(): void;
    w: number;
    readonly isEmpty: boolean;
    readonly copy: Vector;
    static readonly empty: Vector;
    static readonly schema: string[];
    static getArray(...args: number[]): number[];
}
