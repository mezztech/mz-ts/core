import ISphere from "../interfaces/iSphere";
import Point3D from "./point3D";
export default class Sphere extends Point3D implements ISphere {
    constructor(...values: any[]);
    protected _setSchema(): void;
    radius: number;
    readonly isEmpty: boolean;
    readonly copy: Sphere;
    static readonly empty: Sphere;
    static readonly schema: string[];
    static getArray(...args: number[]): number[];
}
