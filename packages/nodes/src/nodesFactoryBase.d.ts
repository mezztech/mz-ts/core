import INode from './interfaces/iNode';
import INodeFactory from './interfaces/iNodeFactory';
import INodesFactory from './interfaces/iNodesFactory';
import INodes from './interfaces/iNodes';
export declare abstract class NodesFactoryBase<TNodes extends INodes> implements INodesFactory {
    constructor(nodeFactory: INodeFactory);
    readonly node: INodeFactory;
    protected p_node: INodeFactory;
    instance(parent?: INode, ...args: any[]): TNodes;
    fromAny(value: (INode | TNodes | Object | string | Array<any>), parent?: INode, ...args: any[]): TNodes;
    fromString(value: string, parent?: INode, parseString?: (data: string) => TNodes, ...args: any[]): TNodes;
    fromJson(value: string, parent?: INode, ...args: any[]): TNodes;
    fromObject(value: Object, parent?: INode, ...args: any[]): TNodes;
    fromArray(value: any[], parent?: INode, ...args: any[]): TNodes;
}
export default NodesFactoryBase;
