import NodeChangeOptions from './nodeChangeOptions';
import INodeChangeOptions from './interfaces/iNodeChangeOptions';
import INodeCreateOptions from './interfaces/iNodeCreateOptions';


export class NodeCreateOptions implements INodeCreateOptions {
    /**
     * @param {string} key A unique ID
     * @param {string} label A user-friendly label
     * @param {number} index
     * @param {INodeChangeOptions} changeOptions
     */
    constructor(
        label?:string,
        key?:string,
        index?:number,
        changeOptions:INodeChangeOptions = NodeChangeOptions.default
    ) {
        this.key = key;
        this.label = label;
        this.index = index;
        this.changeOptions = changeOptions;
    }


    key:string;
    label:string;
    index:number;
    changeOptions:INodeChangeOptions;
} // End class


export default NodeCreateOptions;
