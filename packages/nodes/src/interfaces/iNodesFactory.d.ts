import INode from './iNode';
import INodes from './iNodes';
export interface INodesFactory {
    instance(...args: any[]): INodes;
    fromAny(value: (INodes | Object | string), parent?: INode, ...args: any[]): INodes;
    fromString(value: string, parent?: INode, parseString?: (value: string) => INodes, ...args: any[]): INodes;
    fromJson(value: string, parent?: INode, ...args: any[]): INodes;
    fromObject(value: Object, parent?: INode, ...args: any[]): INodes;
    fromArray(value: any[], parent?: INode, ...args: any[]): INodes;
}
export default INodesFactory;
