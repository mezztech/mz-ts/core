import IData from './iData'


export interface IDataOwner {
    data:IData;
} // End interface


export default IDataOwner;
