export enum KeyAutoGenerationType {
    NONE,
    GLOBALLY_UNIQUE,
    SESSION_UNIQUE
}
