import { DataType } from './attributes/attributeEnums';
export declare class DataTools {
    static getDataType(value: any): (DataType | string);
    static setCookie(name: string, value: string, expireMilliseconds?: number): void;
    static getCookie(name: string): string;
}
export default DataTools;
