"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var KeyAutoGenerationType;
(function (KeyAutoGenerationType) {
    KeyAutoGenerationType[KeyAutoGenerationType["NONE"] = 0] = "NONE";
    KeyAutoGenerationType[KeyAutoGenerationType["GLOBALLY_UNIQUE"] = 1] = "GLOBALLY_UNIQUE";
    KeyAutoGenerationType[KeyAutoGenerationType["SESSION_UNIQUE"] = 2] = "SESSION_UNIQUE";
})(KeyAutoGenerationType = exports.KeyAutoGenerationType || (exports.KeyAutoGenerationType = {}));
//# sourceMappingURL=dataEnums.js.map