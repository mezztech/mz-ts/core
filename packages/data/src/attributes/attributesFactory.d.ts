import Attributes from './attributes';
import AttributesFactoryBase from './attributesFactoryBase';
import IAttribute from './interfaces/iAttribute';
export declare class AttributesFactory extends AttributesFactoryBase<Attributes> {
    instance(parent?: IAttribute, ...args: any[]): Attributes;
}
export default AttributesFactory;
