import Attributes from './attributes';
import AttributeBase from './attributeBase';
import IAttributeFactory from './interfaces/iAttributeFactory';
import IAttributes from "./interfaces/iAttributes";
export declare class AttributeRoot extends AttributeBase<Attributes, AttributeBase<any, any, any, any>, Attributes, AttributeRoot> {
    constructor(data?: (IAttributes | Object | string), key?: string, label?: string);
    static readonly create: IAttributeFactory;
    private static _factory;
}
export default AttributeRoot;
