import AttributesBase from './attributesBase';
import AttributeBase from './attributeBase';
import Attribute from './attribute';


export class Attributes extends AttributesBase<AttributeBase<any, any, any, any>, AttributesBase<any, any>> {
    constructor(parent?:any) { super(Attribute.create, parent, '.'); }
} // End class


export default Attributes;
