import AttributesBase from './attributesBase';
import AttributeBase from './attributeBase';
export declare class Attributes extends AttributesBase<AttributeBase<any, any, any, any>, AttributesBase<any, any>> {
    constructor(parent?: any);
}
export default Attributes;
