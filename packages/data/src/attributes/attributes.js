"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const attributesBase_1 = require("./attributesBase");
const attribute_1 = require("./attribute");
class Attributes extends attributesBase_1.default {
    constructor(parent) { super(attribute_1.default.create, parent, '.'); }
} // End class
exports.Attributes = Attributes;
exports.default = Attributes;
//# sourceMappingURL=attributes.js.map