import { NodeType } from '@mezzy/nodes';
import Attribute from '../attribute';
import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
import { DataType } from '../attributeEnums';


export class AttributeNumbers
extends AttributeBase<Array<number>, AttributeNumbers, Attributes, AttributeRoot> {
    constructor(value:any[] = [], label?:string, key?:string) {
        super(Attribute.create, value, DataType.NUMBERS, label, key, NodeType.BASIC, true);
    }
} // End class


export default AttributeNumbers;
