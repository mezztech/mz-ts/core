"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const attributeEnums_1 = require("../attributeEnums");
const attributeNumberBase_1 = require("./attributeNumberBase");
class AttributeDouble extends attributeNumberBase_1.default {
    constructor(value = 0.0, label, key) {
        super(value, attributeEnums_1.DataType.NUMBER_DOUBLE, label, key);
    }
    static create(options) {
        return new AttributeDouble(options.value, options.label, options.key);
    }
} // End class
exports.AttributeDouble = AttributeDouble;
exports.default = AttributeDouble;
//# sourceMappingURL=attributeDouble.js.map