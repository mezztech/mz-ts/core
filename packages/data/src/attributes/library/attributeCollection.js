"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodes_1 = require("@mezzy/nodes");
const attributeEnums_1 = require("../attributeEnums");
const attribute_1 = require("../attribute");
const attributeBase_1 = require("../attributeBase");
const dataTools_1 = require("../../dataTools");
class AttributeCollection extends attributeBase_1.default {
    constructor(data, dataType = attributeEnums_1.DataType.COLLECTION, label, key, isRoot = false, isInitialize = true) {
        super(attribute_1.default.create, null, attributeEnums_1.DataType.COLLECTION, label, key, nodes_1.NodeType.COLLECTION, false);
        this._isRoot = isRoot;
        if (isInitialize)
            this.initialize(data, dataType);
    }
    initialize(data, dataType = attributeEnums_1.DataType.COLLECTION) {
        if (data) {
            let dataTypeDetected = dataTools_1.default.getDataType(data);
            if (this.create.getBaseDataType(dataTypeDetected) === attributeEnums_1.DataType.COLLECTION) {
                dataType = dataTypeDetected;
            }
        }
        return super.initialize(data, dataType, this._isRoot ? nodes_1.NodeType.ROOT : nodes_1.NodeType.COLLECTION);
    }
} // End class
exports.AttributeCollection = AttributeCollection;
exports.default = AttributeCollection;
//# sourceMappingURL=attributeCollection.js.map