import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
export declare class AttributeNumbers extends AttributeBase<Array<number>, AttributeNumbers, Attributes, AttributeRoot> {
    constructor(value?: any[], label?: string, key?: string);
}
export default AttributeNumbers;
