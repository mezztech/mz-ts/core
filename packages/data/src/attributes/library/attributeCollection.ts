import { NodeType } from '@mezzy/nodes';
import { IPromise } from "@mezzy/result";
import { DataType} from '../attributeEnums';
import Attribute from '../attribute';
import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
import IAttributes from '../interfaces/iAttributes'
import DataTools from '../../dataTools';


export class AttributeCollection
extends AttributeBase<Attributes, AttributeCollection, Attributes, AttributeRoot> {
    constructor(
        data?:(IAttributes | Object | string | Array<any>),
        dataType:string = DataType.COLLECTION,
        label?:string,
        key?:string,
        isRoot:boolean = false,
        isInitialize:boolean = true
    ) {
        super(Attribute.create, null, DataType.COLLECTION, label, key, NodeType.COLLECTION, false);
        this._isRoot = isRoot;
        if (isInitialize) this.initialize(data, dataType);
    }


    initialize(data?:(IAttributes | Object | string | Array<any>), dataType:string = DataType.COLLECTION):IPromise<any> {
        if (data) {
            let dataTypeDetected:string = DataTools.getDataType(data);
            if (this.create.getBaseDataType(dataTypeDetected) === DataType.COLLECTION) {
                dataType = dataTypeDetected;
            }
        }
        return super.initialize(data, dataType, this._isRoot ? NodeType.ROOT : NodeType.COLLECTION);
    }


    private _isRoot:boolean;


    // get attributes():Attributes { return <Attributes>this.p_value; }
} // End class


export default AttributeCollection;
