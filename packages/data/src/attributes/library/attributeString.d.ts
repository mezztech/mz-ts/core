import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
import { DataType } from '../attributeEnums';
export declare abstract class AttributeStringBase extends AttributeBase<string, AttributeStringBase, Attributes, AttributeRoot> {
    constructor(value?: string, dataType?: DataType, label?: string, key?: string);
}
export declare class AttributeString extends AttributeStringBase {
    constructor(value?: string, label?: string, key?: string);
}
export declare class AttributeStringJson extends AttributeStringBase {
    constructor(value?: string, label?: string, key?: string);
}
export declare class AttributeStringBase64 extends AttributeStringBase {
    constructor(value?: string, label?: string, key?: string);
}
export declare class AttributeStringBinary extends AttributeStringBase {
    constructor(value?: string, label?: string, key?: string);
}
export declare class AttributeStringDataUrl extends AttributeStringBase {
    constructor(value?: string, label?: string, key?: string);
}
export declare class AttributeStringHexValue extends AttributeStringBase {
    constructor(value?: string, label?: string, key?: string);
}
export declare class AttributeStringHtml extends AttributeStringBase {
    constructor(value?: string, label?: string, key?: string);
}
export declare class AttributeStringJavascript extends AttributeStringBase {
    constructor(value?: string, label?: string, key?: string);
}
export declare class AttributeStringRegExp extends AttributeStringBase {
    constructor(value?: string, label?: string, key?: string);
}
export declare class AttributeStringSvg extends AttributeStringBase {
    constructor(value?: string, label?: string, key?: string);
}
export declare class AttributeStringUri extends AttributeStringBase {
    constructor(value?: string, label?: string, key?: string);
}
export declare class AttributeStringXml extends AttributeStringBase {
    constructor(value?: string, label?: string, key?: string);
}
export default AttributeString;
