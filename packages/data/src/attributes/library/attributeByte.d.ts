import AttributeNumberBase from './attributeNumberBase';
export declare class AttributeByte extends AttributeNumberBase {
    constructor(value?: number, label?: string, key?: string);
}
export default AttributeByte;
