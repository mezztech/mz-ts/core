import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
export declare class AttributeRegExp extends AttributeBase<RegExp, AttributeRegExp, Attributes, AttributeRoot> {
    constructor(value?: RegExp, label?: string, key?: string);
}
export default AttributeRegExp;
