import { DataType } from '../attributeEnums';
import AttributeNumberBase from './attributeNumberBase';


export class AttributeInt extends AttributeNumberBase {
    constructor(value:number = 0, label?:string, key?:string) {
        super(value, DataType.NUMBER_INT, label, key);
        this.p_formatter = (value:number) => { return Math.round(value); };
    }
} // End class


export default AttributeInt;
