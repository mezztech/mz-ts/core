import { IPromise } from "@mezzy/result";
import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
import IAttributes from '../interfaces/iAttributes';
export declare class AttributeCollection extends AttributeBase<Attributes, AttributeCollection, Attributes, AttributeRoot> {
    constructor(data?: (IAttributes | Object | string | Array<any>), dataType?: string, label?: string, key?: string, isRoot?: boolean, isInitialize?: boolean);
    initialize(data?: (IAttributes | Object | string | Array<any>), dataType?: string): IPromise<any>;
    private _isRoot;
}
export default AttributeCollection;
