"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const attributeEnums_1 = require("../attributeEnums");
const attributeNumberBase_1 = require("./attributeNumberBase");
class AttributeInt extends attributeNumberBase_1.default {
    constructor(value = 0, label, key) {
        super(value, attributeEnums_1.DataType.NUMBER_INT, label, key);
        this.p_formatter = (value) => { return Math.round(value); };
    }
} // End class
exports.AttributeInt = AttributeInt;
exports.default = AttributeInt;
//# sourceMappingURL=attributeInt.js.map