import { DataType } from '../attributeEnums';
import AttributeNumberBase from './attributeNumberBase';
import AttributeCreateOptions from '../attributeCreateOptions';


export class AttributeDouble extends AttributeNumberBase {
    constructor(value:number = 0.0, label?:string, key?:string) {
        super(value, DataType.NUMBER_DOUBLE, label, key);
    }


    static create(options:AttributeCreateOptions):AttributeDouble {
        return new AttributeDouble(options.value, options.label, options.key);
    }
} // End class


export default AttributeDouble;
