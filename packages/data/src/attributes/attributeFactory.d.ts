import AttributeFactoryBase from './attributeFactoryBase';
import IAttributesFactory from './interfaces/iAttributesFactory';
import IAttribute from './interfaces/iAttribute';
import IAttributeFactory from './interfaces/iAttributeFactory';
export declare class AttributeFactory extends AttributeFactoryBase<IAttribute> implements IAttributeFactory {
    constructor();
    readonly collection: IAttributesFactory;
    protected p_collection: IAttributesFactory;
}
export default AttributeFactory;
