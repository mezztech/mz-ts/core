"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AttributeReader {
    constructor(attribute) {
        this.p_attribute = attribute;
    }
    get value() { return this.p_attribute.value; }
    get valuePrevious() { return this.p_attribute.valuePrevious; }
    get changed() { return this.p_attribute.changed; }
} // End class
exports.AttributeReader = AttributeReader;
exports.default = AttributeReader;
//# sourceMappingURL=attributeReader.js.map